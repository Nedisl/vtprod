'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const Country = require('./country');

const modelDefinition = {
    country: {
        type: Sequelize.INTEGER,
        references: {model: 'Countries', key: 'id'},
    },
    month: {
        type: Sequelize.INTEGER,
    },
    description: {
        type: Sequelize.STRING,
    },
    scriptCode: {
        type: Sequelize.STRING,
    },
    photo: {
        type: Sequelize.STRING,
    },
    descriptionPhoto: {
        type: Sequelize.STRING,
    },
    meta_title: {
        type: Sequelize.STRING,
    },
    meta_description: {
        type: Sequelize.STRING,
    },
    meta_keywords: {
        type: Sequelize.STRING,
    },
    header: {
        type: Sequelize.STRING,
    },
    isOutput: {
        type: Sequelize.INTEGER,
    },
};

const MonthModel = db.define('Months', modelDefinition);

MonthModel.belongsTo(Country, {
    foreignKey: 'country',
});
module.exports = MonthModel;
