'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    visa: {
        type: Sequelize.INTEGER,
        references: {model: 'Visa', key: 'id'},
    },
    picture: {
        type: Sequelize.STRING,
    },
};

const VisaPhotoModel = db.define('VisaPhotos', modelDefinition);

module.exports = VisaPhotoModel;
