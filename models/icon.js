'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
    path: {
        type: Sequelize.DataTypes.DOUBLE,
    },
};

const IconModel = db.define('Icons', modelDefinition);

module.exports = IconModel;
