'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    text: {
        type: Sequelize.STRING,
    },
    identifier: {
        type: Sequelize.STRING,
    },
    url: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_title: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_description: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_keywords: {
        type: Sequelize.DataTypes.STRING,
    },
    name: {
        type: Sequelize.DataTypes.STRING,
    },
    header: {
        type: Sequelize.STRING,
    },
};

const PageModel = db.define('Pages', modelDefinition);

module.exports = PageModel;
