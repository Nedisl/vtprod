'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    email: {
        type: Sequelize.STRING,
    },
};

const SubscriberModel = db.define('Subscribers', modelDefinition);

module.exports = SubscriberModel;
