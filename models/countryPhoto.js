'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    country: {
        type: Sequelize.INTEGER,
        references: {model: 'Countries', key: 'id'},
    },
    picture: {
        type: Sequelize.STRING,
    },
};

const CountryPhotoModel = db.define('CountryPhotos', modelDefinition);

module.exports = CountryPhotoModel;
