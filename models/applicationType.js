'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
};

const ApplicationTypeModel = db.define('ApplicationTypes', modelDefinition);

module.exports = ApplicationTypeModel;
