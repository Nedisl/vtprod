'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

/**
 * в данный момент просто заложены,
 * но не используются поля url, rating, header и серия meta_*,
 */
const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
    price: {
        type: Sequelize.STRING,
    },
    pictures: {
        type: Sequelize.TEXT,
    },
    desc: {
        type: Sequelize.TEXT,
    },
    rating: {
        type: Sequelize.INTEGER,
        default: 5,
    },
    url: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_title: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_description: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_keywords: {
        type: Sequelize.DataTypes.STRING,
    },
    header: {
        type: Sequelize.STRING,
    },
};

const Hotel = db.define('Hotel', modelDefinition);

module.exports = Hotel;
