'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const ApplicationType = require('./applicationType');
const Tour = require('./tour');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
    phone: {
        type: Sequelize.STRING,
    },
    email: {
        type: Sequelize.STRING,
    },
    tour: {
        type: Sequelize.INTEGER,
    },
    applicationType: {
        type: Sequelize.INTEGER,
        references: {model: 'ApplicationTypes', key: 'id'},
    },
    companyName: {
        type: Sequelize.STRING,
    },
    country: {
        type: Sequelize.STRING,
    },
};

const ApplicationModel = db.define('Applications', modelDefinition);

ApplicationModel.belongsTo(ApplicationType, {
    foreignKey: 'applicationType',
});
ApplicationModel.belongsTo(Tour, {
    foreignKey: 'tour',
});

module.exports = ApplicationModel;
