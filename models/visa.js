'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const Country = require('./country');

const modelDefinition = {
    title: {
        type: Sequelize.STRING,
    },
    content: {
        type: Sequelize.STRING,
    },
    price: {
        type: Sequelize.DataTypes.DECIMAL,
    },
    days: {
        type: Sequelize.DataTypes.INTEGER,
    },
    picture: {
        type: Sequelize.DataTypes.STRING,
    },
    isPopular: {
        type: Sequelize.DataTypes.INTEGER,
    },
    country: {
        type: Sequelize.INTEGER,
        references: {model: 'Countries', key: 'id'},
    },
    url: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_title: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_description: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_keywords: {
        type: Sequelize.DataTypes.STRING,
    },
    header: {
        type: Sequelize.STRING,
    },
};

const VisaModel = db.define('Visa', modelDefinition);

VisaModel.belongsTo(Country, {
    foreignKey: 'country',
});

module.exports = VisaModel;
