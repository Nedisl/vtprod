'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    blog: {
        type: Sequelize.INTEGER,
        references: {model: 'Blogs', key: 'id'},
    },
    picture: {
        type: Sequelize.STRING,
    },
};

const BlogPhotoModel = db.define('BlogPhotos', modelDefinition);

module.exports = BlogPhotoModel;
