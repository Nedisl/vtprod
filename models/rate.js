'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    comment: {
        type: Sequelize.STRING,
    },
    param1: {
        type: Sequelize.INTEGER,
    },
    param2: {
        type: Sequelize.INTEGER,
    },
    param3: {
        type: Sequelize.INTEGER,
    },
};

const RateModel = db.define('Rates', modelDefinition);

module.exports = RateModel;
