'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const Team = require('./team');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
    member: {
        type: Sequelize.INTEGER,
        references: {model: 'Teams', key: 'id'},
    },
    title: {
        type: Sequelize.STRING,
    },
    text: {
        type: Sequelize.STRING,
    },
};

const QuoteModel = db.define('Quotes', modelDefinition);

QuoteModel.belongsTo(Team, {
    foreignKey: 'member',
});

module.exports = QuoteModel;
