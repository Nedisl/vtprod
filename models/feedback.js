'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const Office = require('./office');

const modelDefinition = {
    comment: {
        type: Sequelize.STRING,
    },
    office: {
        type: Sequelize.INTEGER,
        references: {model: 'Offices', key: 'id', onDelete: 'SET NULL'},
    },
    type: {
        type: Sequelize.INTEGER,
    },
    city: {
        type: Sequelize.STRING,
    },
    phone: {
        type: Sequelize.STRING,
    },
    email: {
        type: Sequelize.STRING,
    },
    name: {
        type: Sequelize.STRING,
    },
};

const FeedbackModel = db.define('Feedbacks', modelDefinition);

FeedbackModel.belongsTo(Office, {
    foreignKey: 'office',
    onDelete: 'SET NULL',
});

module.exports = FeedbackModel;
