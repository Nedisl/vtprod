'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const Country = require('./country');

const modelDefinition = {
    description: {
        type: Sequelize.STRING,
    },
    title: {
        type: Sequelize.STRING,
    },
    country: {
        type: Sequelize.INTEGER,
        references: {model: 'Countries', key: 'id'},
    },
    picture: {
        type: Sequelize.STRING,
    },
};

const SliderMainModel = db.define('MainSliders', modelDefinition);

SliderMainModel.belongsTo(Country, {
    foreignKey: 'country',
});

module.exports = SliderMainModel;
