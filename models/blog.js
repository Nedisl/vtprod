'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    title: {
        type: Sequelize.STRING,
    },
    type: {
        type: Sequelize.INTEGER,
    },
    fixed: {
        type: Sequelize.INTEGER,
    },
    content: {
        type: Sequelize.STRING,
    },
    picture: {
        type: Sequelize.STRING,
    },
    text: {
        type: Sequelize.DataTypes.STRING,
    },
    url: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_title: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_description: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_keywords: {
        type: Sequelize.DataTypes.STRING,
    },
    header: {
        type: Sequelize.STRING,
    },
};

const BlogModel = db.define('Blogs', modelDefinition);

module.exports = BlogModel;
