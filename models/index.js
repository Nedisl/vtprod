// export * from './application';
// export * from './applicationType';
// export * from './block';
// export * from './blockPhoto';
// export * from './blog';
// export * from './blogPhoto';
// export * from './busTour';
// export * from './calculation';
// export * from './city';
// export * from './contactFaces';
// export * from './country';
// export * from './countryPhoto';
// export * from './feedback';
// export * from './hotel';
// export * from './icon';
// export * from './month';
// export * from './office';
// export * from './page';
// export * from './place';
// export * from './question';
// export * from './quote';
// export * from './rate';
// export * from './review';
// export * from './sliderMain';
// export * from './subscribers';
// export * from './team';
// export * from './tour';
// export * from './tourPhoto';
// export * from './tourType';
// export * from './user';
// export * from './visa';
// export * from './visaPhoto';

module.exports = {
    Hotel: require('./hotel'),
    TourHotel: require('./merge/tour-hotel'),
};
