'use strict';

const Sequelize = require('sequelize');

const bcrypt = require('bcrypt');

const config = require('../config');

const db = require('../services/database');

const modelDefinition = {
    userType: {
        type: Sequelize.INTEGER,
        defaultValue: config.userRoles.user,
    },
    contactName: {
        type: Sequelize.STRING,
        unique: true,
    },
    orgName: {
        type: Sequelize.STRING,
        unique: true,
    },
    city: {
        type: Sequelize.INTEGER,
        references: {model: 'Cities', key: 'id'},
    },
    phone: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    avatar: {
        type: Sequelize.STRING,
    },
};
const modelOptions = {
    hooks: {
        beforeValidate: hashPassword,
    },
};
const UserModel = db.define('Users', modelDefinition, modelOptions);

UserModel.prototype.comparePasswords = comparePasswords;

function comparePasswords(password, callback) {
    bcrypt.compare(password, this.password, function(error, isMatch) {
        if (error) {
            return callback(error);
        }

        return callback(null, isMatch);
    });
}

function hashPassword(user) {
    if (user.changed('password')) {
        return bcrypt.hash(user.password, 10).then(function(password) {
            user.password = password;
        });
    }
}

module.exports = UserModel;
