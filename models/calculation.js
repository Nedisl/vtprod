'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const Country = require('./country');

const modelDefinition = {
    country: {
        type: Sequelize.INTEGER,
        references: {model: 'Countries', key: 'id'},
    },
    nightsFrom: {
        type: Sequelize.INTEGER,
    },
    nightsTo: {
        type: Sequelize.INTEGER,
    },
    date: {
        type: Sequelize.DataTypes.DATE,
    },
    plusMinusOk: {
        type: Sequelize.INTEGER,
    },
    adults: {
        type: Sequelize.INTEGER,
    },
    childs: {
        type: Sequelize.INTEGER,
    },
    phone: {
        type: Sequelize.STRING,
    },
    email: {
        type: Sequelize.STRING,
    },
};

const CalculationModel = db.define('Calculations', modelDefinition);

CalculationModel.belongsTo(Country, {
    foreignKey: 'country',
});

module.exports = CalculationModel;
