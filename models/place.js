'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
    countryCode: {
        type: Sequelize.STRING,
    },
    lat: {
        type: Sequelize.DataTypes.DOUBLE,
    },
    lng: {
        type: Sequelize.DataTypes.DOUBLE,
    },
    description: {
        type: Sequelize.STRING,
    },
    scriptCode: {
        type: Sequelize.STRING,
    },
    url: {
        type: Sequelize.STRING,
    },
    meta_title: {
        type: Sequelize.STRING,
    },
    meta_description: {
        type: Sequelize.STRING,
    },
    meta_keywords: {
        type: Sequelize.STRING,
    },
    header: {
        type: Sequelize.STRING,
    },
    photo: {
        type: Sequelize.STRING,
    },
    descriptionPhoto: {
        type: Sequelize.STRING,
    },
    previewPhoto: {
        type: Sequelize.INTEGER,
    },
    price: {
        type: Sequelize.INTEGER,
    },
    isOutput: {
        type: Sequelize.INTEGER,
    },
};

const PlaceModel = db.define('Places', modelDefinition);

module.exports = PlaceModel;
