'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    tour: {
        type: Sequelize.INTEGER,
        references: {model: 'Tours', key: 'id'},
    },
    photo: {
        type: Sequelize.STRING,
    },
};

const TourPhotoModel = db.define('TourPhotos', modelDefinition);

module.exports = TourPhotoModel;
