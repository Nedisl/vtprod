'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const Place = require('./place');
const TourType = require('./tourType');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
    days: {
        type: Sequelize.STRING,
    },
    includeList: {
        type: Sequelize.STRING,
    },
    notIncludeList: {
        type: Sequelize.STRING,
    },
    pictures: {
        type: Sequelize.STRING,
    },
    hotels: {
        type: Sequelize.STRING,
    },
    banner: {
        type: Sequelize.STRING,
    },
    price: {
        type: Sequelize.DataTypes.DECIMAL,
    },
    priority: {
        type: Sequelize.INTEGER,
    },
    place: {
        type: Sequelize.INTEGER,
        references: {model: 'Places', key: 'id'},
    },
    nights: {
        type: Sequelize.INTEGER,
    },
    selected: {
        type: Sequelize.INTEGER,
    },
    busSelected: {
        type: Sequelize.INTEGER,
    },
    costs: {
        type: Sequelize.STRING,
    },
    tourType: {
        type: Sequelize.INTEGER,
        references: {model: 'TourTypes', key: 'id'},
    },
    discount: {
        type: Sequelize.STRING,
    },
    url: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_title: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_description: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_keywords: {
        type: Sequelize.DataTypes.STRING,
    },
    header: {
        type: Sequelize.STRING,
    },
};

const TourModel = db.define('Tours', modelDefinition);

TourModel.belongsTo(Place, {foreignKey: 'place'});
TourModel.belongsTo(TourType, {foreignKey: 'tourType'});

module.exports = TourModel;
