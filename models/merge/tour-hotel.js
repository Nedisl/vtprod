'use strict';

const Sequelize = require('sequelize');

const Tour = require('../tour');
const Hotel = require('../hotel');
const db = require('../../services/database');

const TourHotel = db.define('TourHotel', {
    TourId: {
        type: Sequelize.INTEGER,
        references: {
            model: Tour,
        },
    },
    HotelId: {
        type: Sequelize.INTEGER,
        references: {
            model: Hotel,
            key: 'id',
        },
    },
});

Tour.belongsToMany(Hotel, {through: TourHotel});
Hotel.belongsToMany(Tour, {through: TourHotel});

module.exports = TourHotel;
