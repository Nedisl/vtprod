'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const Quote = require('./quote');
const Team = require('./team');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
    lat: {
        type: Sequelize.DataTypes.DOUBLE,
    },
    lng: {
        type: Sequelize.DataTypes.DOUBLE,
    },
    flagPicture: {
        type: Sequelize.STRING,
    },
    blockVisa: {
        type: Sequelize.STRING,
    },
    guide: {
        type: Sequelize.STRING,
    },
    preview: {
        type: Sequelize.STRING,
    },
    hotels: {
        type: Sequelize.STRING,
    },
    scriptCode: {
        type: Sequelize.STRING,
    },
    code: {
        type: Sequelize.STRING,
    },
    weather: {
        type: Sequelize.STRING,
    },
    background: {
        type: Sequelize.STRING,
    },
    startPrice: {
        type: Sequelize.INTEGER,
    },
    isPopular: {
        type: Sequelize.INTEGER,
    },
    selected: {
        type: Sequelize.INTEGER,
    },
    available: {
        type: Sequelize.INTEGER,
    },
    quote: {
        type: Sequelize.INTEGER,
        references: {model: 'Quotes', key: 'id'},
    },
    url: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_title: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_description: {
        type: Sequelize.DataTypes.STRING,
    },
    meta_keywords: {
        type: Sequelize.DataTypes.STRING,
    },
    header: {
        type: Sequelize.STRING,
    },
};

const CountryModel = db.define('Countries', modelDefinition);

CountryModel.belongsTo(Quote, {
    foreignKey: 'quote',
});

module.exports = CountryModel;
