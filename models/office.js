'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const City = require('./city');

const modelDefinition = {
    title: {
        type: Sequelize.STRING,
    },
    info: {
        type: Sequelize.STRING,
    },
    address: {
        type: Sequelize.STRING,
    },
    busStop: {
        type: Sequelize.STRING,
    },
    phone: {
        type: Sequelize.STRING,
    },
    mail: {
        type: Sequelize.STRING,
    },
    schedule: {
        type: Sequelize.STRING,
    },
    picture: {
        type: Sequelize.STRING,
    },
    city: {
        type: Sequelize.INTEGER,
        references: {model: 'Cities', key: 'id'},
    },
};

const OfficeModel = db.define('Office', modelDefinition);

OfficeModel.belongsTo(City, {foreignKey: 'city'});

module.exports = OfficeModel;
