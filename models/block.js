'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
    description: {
        type: Sequelize.STRING,
    },
};

const BlockModel = db.define('Blocks', modelDefinition);

module.exports = BlockModel;
