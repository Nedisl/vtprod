'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
};

const CityModel = db.define('Cities', modelDefinition);

module.exports = CityModel;
