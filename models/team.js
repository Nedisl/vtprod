'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const Office = require('./office');

const modelDefinition = {
    picture: {
        type: Sequelize.STRING,
    },
    pictureSmall: {
        type: Sequelize.STRING,
    },
    name: {
        type: Sequelize.STRING,
    },
    post: {
        type: Sequelize.STRING,
    },
    phone: {
        type: Sequelize.STRING,
    },
    additionPhone: {
        type: Sequelize.STRING,
    },
    office: {
        type: Sequelize.INTEGER,
        references: {model: 'Offices', key: 'id'},
    },
    position: {
        type: Sequelize.INTEGER,
    },
};

const TeamModel = db.define('Team', modelDefinition);

TeamModel.belongsTo(Office, {
    foreignKey: 'office',
    onDelete: 'SET NULL',
});

module.exports = TeamModel;
