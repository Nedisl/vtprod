'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    name: {
        type: Sequelize.STRING,
    },
};

const TourTypeModel = db.define('TourTypes', modelDefinition);

module.exports = TourTypeModel;
