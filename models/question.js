'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    comment: {
        type: Sequelize.STRING,
    },
    phone: {
        type: Sequelize.STRING,
    },
    name: {
        type: Sequelize.STRING,
    },
};

const QuestionModel = db.define('Questions', modelDefinition);

module.exports = QuestionModel;
