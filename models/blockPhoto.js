'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    block: {
        type: Sequelize.INTEGER,
        references: {model: 'Blocks', key: 'id'},
    },
    picture: {
        type: Sequelize.STRING,
    },
};

const BlockPhotoModel = db.define('BlockPhotos', modelDefinition);

module.exports = BlockPhotoModel;
