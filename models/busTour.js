'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');

const modelDefinition = {
    title: {
        type: Sequelize.STRING,
    },
    bannerTop: {
        type: Sequelize.STRING,
    },
    bannerBottom: {
        type: Sequelize.STRING,
    },
    text: {
        type: Sequelize.STRING,
    },
};

const BusTourModel = db.define('BusTours', modelDefinition);

module.exports = BusTourModel;
