'use strict';

const Sequelize = require('sequelize');

const db = require('../services/database');
const Country = require('./country');

const modelDefinition = {
    comment: {
        type: Sequelize.STRING,
    },
    name: {
        type: Sequelize.STRING,
    },
    country: {
        type: Sequelize.INTEGER,
        references: {model: 'Countries', key: 'id'},
    },
    place: {
        type: Sequelize.INTEGER,
        references: {model: 'Places', key: 'id'},
    },
    reviewType: {
        type: Sequelize.INTEGER,
    },
    onlyAva: {
        type: Sequelize.INTEGER,
    },
    isPublished: {
        type: Sequelize.INTEGER,
    },
    rating: {
        type: Sequelize.INTEGER,
    },
    date: {
        type: Sequelize.STRING,
    },
    code: {
        type: Sequelize.STRING,
    },
    pictures: {
        type: Sequelize.STRING,
    },
};

const ReviewModel = db.define('Reviews', modelDefinition);

ReviewModel.belongsTo(Country, {
    foreignKey: 'country',
});

module.exports = ReviewModel;
