'use strict';

const Blog = require('../models/blog');
const BlogPhoto = require('../models/blogPhoto');
const Sequelize = require('sequelize');
const BlogController = {};

/**
 * Add new blog (admin only)
 * @param req Request data
 * @param res Response data
 */
BlogController.addBlog = (req, res) => {
    if (!req.body.title || !req.body.content || !req.body.type || !req.body.picture) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Blog.create(req.body).then(blog => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Blog added!',
                        },
                        data: blog,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating blogs!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Add new blog (admin only)
 * @param req Request data
 * @param res Response data
 */
BlogController.addBlogPhoto = (req, res) => {
    if (!req.body.picture || !req.body.blog) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return BlogPhoto.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Blog photo added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating blog photos!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all blogs
 * @param req Request data
 * @param res Response data
 */
BlogController.getBlogs = (req, res) => {
    Promise.resolve()
        .then(function() {
            Blog.findAll().then(function(blogs) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got blogs!",
                    },
                    data: {
                        blogs,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get blogs!',
                },
                data: null,
            });
        });
};

/**
 * Get all blogs
 * @param req Request data
 * @param res Response data
 */
BlogController.getFixedBlogs = (req, res) => {
    Promise.resolve()
        .then(function() {
            Blog.findAll({
                where: {
                    fixed: req.params.fixed,
                },
            }).then(function(blogs) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got blogs!",
                    },
                    data: {
                        blogs,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get blogs!',
                },
                data: null,
            });
        });
};

/**
 * Get random blogs
 * @param req Request data
 * @param res Response data
 */
BlogController.getRandomBlogs = (req, res) => {
    Promise.resolve()
        .then(function() {
            Blog.findAll({
                where: {
                    id: {
                        ne: req.params.id,
                    },
                },
                order: Sequelize.literal('rand()'),
                limit: 4,
            }).then(function(blogs) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got blogs!",
                    },
                    data: {
                        blogs,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get blogs!',
                },
                data: null,
            });
        });
};
/**
 * Get all blogs
 * @param req Request data
 * @param res Response data
 */
BlogController.getBlogPhotos = (req, res) => {
    Promise.resolve()
        .then(function() {
            BlogPhoto.findAll({
                where: {
                    blog: req.params.id,
                },
            }).then(function(blogs) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got blogs!",
                    },
                    data: {
                        blogs,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get blogs!',
                },
                data: null,
            });
        });
};

/**
 * Get blog by id
 * @param req Request data
 * @param res Response data
 */
BlogController.getBlog = (req, res) => {
    Promise.resolve()
        .then(function() {
            Blog.findByPk(req.params.id).then(function(blog) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got blog!",
                    },
                    data: blog,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get blogs!',
                },
                data: null,
            });
        });
};

/**
 * Update blog by id
 * @param req Request data
 * @param res Response data
 */
BlogController.updateBlog = (req, res) => {
    Promise.resolve()
        .then(function() {
            Blog.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Blog successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Blog update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Blog update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete blog by id
 * @param req Request data
 * @param res Response data
 */
BlogController.deleteBlog = (req, res) => {
    Promise.resolve()
        .then(function() {
            Blog.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Blog successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Blog delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Blog delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete blog by id
 * @param req Request data
 * @param res Response data
 */
BlogController.deleteBlogPhoto = (req, res) => {
    Promise.resolve()
        .then(function() {
            BlogPhoto.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Blog photo successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Blog photo delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Blog delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = BlogController;
