'use strict';

const Review = require('../models/review');
const Country = require('../models/country');
const moment = require('moment');
const sequelize = require('sequelize');
const {Op} = sequelize;

const ReviewController = {};

/**
 * Add new review (admin only)
 * @param req Request data
 * @param res Response data
 */
ReviewController.addReview = (req, res) => {
    if (
        !req.body.comment ||
        !req.body.country ||
        !req.body.name ||
        !req.body.date ||
        !req.body.rating ||
        req.body.pictures.length < 1
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else if (!moment(req.body.date, 'YYYY-MM-DD', true).isValid()) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Date is invalid.',
            },
            data: null,
        });
    } else {
        if (!req.body.place) {
            req.body.place = 27;
        }
        req.body.reviewType = 1;
        Promise.resolve()
            .then(() => {
                return Review.create(req.body).then(review => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Review added!',
                        },
                        data: review,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating reviews!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all reviews for country
 * @param req Request data
 * @param res Response data
 */
ReviewController.getCountryReviews = (req, res) => {
    Promise.resolve()
        .then(function() {
            Review.findAll({
                where: {
                    country: req.params.id,
                    isPublished: 1,
                },
                include: [
                    {
                        model: Country,
                    },
                ],
            }).then(function(reviews) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got reviews!",
                    },
                    data: {
                        reviews,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get reviews!',
                },
                data: null,
            });
        });
};

/**
 * Get all published paginated reviews
 * @param req Request data
 * @param res Response data
 */
ReviewController.getAllReviews = (req, res) => {
    Promise.resolve()
        .then(function() {
            let offset = 0;
            let limit = 100000;

            if (req.query.offset) {
                offset = +req.query.offset;
            }

            if (req.query.offset) {
                limit = +req.query.limit;
            }

            // if (req.query.date) {
            //     let date = moment(req.query.date);
            //     let nextDate = moment(req.query.date);
            //     nextDate.add(1, 'months');
            //     delete req.query.date;
            //     req.query.date = {
            //         [Op.and]: [{
            //             [Op.gte]: date.format()
            //         }, {
            //             [Op.lt]: nextDate.format()
            //         }]
            //     };
            // }

            if (req.query.date) {
                const {date} = req.query;

                delete req.query.date;
                req.query.date = {
                    [Op.and]: [
                        sequelize.where(
                            sequelize.fn('MONTH', sequelize.col('date')),
                            date,
                        ),
                    ],
                };
            }

            delete req.query.limit;
            delete req.query.offset;

            Review.findAll({
                where: req.query,
                include: [
                    {
                        model: Country,
                    },
                ],
                order: [['createdAt', 'DESC']],
                limit: limit + 1,
                offset,
            }).then(function(reviews) {
                let isLastPage = false;

                if (reviews.length < limit + 1) {
                    isLastPage = true;
                } else {
                    reviews.pop();
                }
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got reviews!",
                        isLastPage,
                    },
                    data: {
                        reviews,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get reviews!',
                },
                data: null,
            });
        });
};
/**
 * Get review by id
 * @param req Request data
 * @param res Response data
 */
ReviewController.getReview = (req, res) => {
    Promise.resolve()
        .then(function() {
            Review.findOne({
                where: {
                    id: req.params.id,
                },
                include: [
                    {
                        model: Country,
                    },
                ],
            }).then(function(review) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got review!",
                    },
                    data: review,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get reviews!',
                },
                data: null,
            });
        });
};

/**
 * Update review by id (admin only)
 * @param req Request data
 * @param res Response data
 */
ReviewController.updateReview = (req, res) => {
    Promise.resolve()
        .then(function() {
            Review.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Review successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Review update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Review update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete review by id (admin only)
 * @param req Request data
 * @param res Response data
 */
ReviewController.deleteReview = (req, res) => {
    Promise.resolve()
        .then(function() {
            Review.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Review successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Review delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Review delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = ReviewController;
