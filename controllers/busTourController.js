'use strict';

const BusTour = require('../models/tour');

const BusTourController = {};

/**
 * Add new tour (admin only)
 * @param req Request data
 * @param res Response data
 */
BusTourController.addTour = (req, res) => {
    if (
        !req.body.title ||
        !req.body.bannerTop ||
        !req.body.bannerBottom ||
        !req.body.text
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return BusTour.create(req.body).then(tour => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Bus Tour added!',
                        },
                        data: tour,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating bus tours!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all tours
 * @param req Request data
 * @param res Response data
 */
BusTourController.getTours = (req, res) => {
    Promise.resolve()
        .then(function() {
            BusTour.findAll().then(function(tours) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tours!",
                    },
                    data: tours,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get tours!',
                },
                data: null,
            });
        });
};
/**
 * Get tour by id
 * @param req Request data
 * @param res Response data
 */
BusTourController.getTour = (req, res) => {
    Promise.resolve()
        .then(function() {
            BusTour.findByPk(req.params.id).then(function(tour) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tour!",
                    },
                    data: tour,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get tours!',
                },
                data: null,
            });
        });
};

/**
 * Update tour by id (admin only)
 * @param req Request data
 * @param res Response data
 */
BusTourController.updateTour = (req, res) => {
    Promise.resolve()
        .then(function() {
            BusTour.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Tour successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Tour update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Tour update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete tour by id (admin only)
 * @param req Request data
 * @param res Response data
 */
BusTourController.deleteTour = (req, res) => {
    Promise.resolve()
        .then(function() {
            BusTour.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Tour successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Tour delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Tour delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = BusTourController;
