'use strict';

const Quote = require('../models/quote');
const Team = require('../models/team');

const QuoteController = {};

/**
 * Add new quote (admin only)
 * @param req Request data
 * @param res Response data
 */
QuoteController.addQuote = (req, res) => {
    if (!req.body.name || !req.body.text || !req.body.member || !req.body.title) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Quote.create(req.body).then(quote => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Quote added!',
                        },
                        data: quote,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating quotes!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all quotes
 * @param req Request data
 * @param res Response data
 */
QuoteController.getQuotes = (req, res) => {
    Promise.resolve()
        .then(() => {
            Quote.findAll({
                include: [
                    {
                        model: Team,
                    },
                ],
            }).then(quotes => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got quotes!",
                    },
                    data: {
                        quotes,
                    },
                });
            });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get quotes!',
                },
                data: null,
            });
        });
};

/**
 * Get quote by id
 * @param req Request data
 * @param res Response data
 */
QuoteController.getQuote = (req, res) => {
    Promise.resolve()
        .then(() => {
            Quote.findOne({
                where: {
                    id: req.params.id,
                },
                include: [
                    {
                        model: Team,
                    },
                ],
            }).then(quote => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got quote!",
                    },
                    data: quote,
                });
            });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get quotes!',
                },
                data: null,
            });
        });
};

/**
 * Update quote by id
 * @param req Request data
 * @param res Response data
 */
QuoteController.updateQuote = (req, res) => {
    Promise.resolve()
        .then(() => {
            Quote.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(() => {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Quote successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(e => {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Quote update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(error => {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Quote update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete quote by id
 * @param req Request data
 * @param res Response data
 */
QuoteController.deleteQuote = (req, res) => {
    Promise.resolve()
        .then(() => {
            Quote.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(() => {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Quote successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(e => {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Quote delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Quote delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = QuoteController;
