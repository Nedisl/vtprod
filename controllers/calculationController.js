'use strict';

const Calculation = require('../models/calculation');
const Country = require('../models/country');
const mail = require('../services/mail');
const CountryService = require('../services/country');

const CalculationController = {};

/**
 * Add new calculation
 * @param req Request data
 * @param res Response data
 */
CalculationController.addCalculation = async (req, res) => {
    if (
        !req.body.country ||
        !req.body.nightsFrom ||
        !req.body.nightsTo ||
        !req.body.hasOwnProperty('plusMinusOk') ||
        !req.body.adults ||
        !req.body.hasOwnProperty('childs') ||
        !(req.body.phone || req.body.email)
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        const country = await CountryService.getById(req.body.country);

        mail.sendNoReplyMessage(
            'Новая заявка с ВяткаТур',
            'Телефон: ' +
                req.body.phone +
                ', E-mail: ' +
                req.body.email +
                ', Страна: ' +
                country.data.name +
                ', Взрослые: ' +
                req.body.adults +
                ', Дети: ' +
                req.body.childs +
                ', Ночей от: ' +
                req.body.nightsFrom +
                ' до ' +
                req.body.nightsTo +
                ' тип заявки: расчет тура, дата: ' +
                req.body.date,
            'zapros@vturs.ru',
        );

        Promise.resolve()
            .then(() => {
                return Calculation.create(req.body).then(calc => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Calculation added!',
                        },
                        data: calc,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating calculations!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all calculations
 * @param req Request data
 * @param res Response data
 */
CalculationController.getCalculations = (req, res) => {
    Promise.resolve()
        .then(function() {
            Calculation.findAll({
                order: [['id', 'DESC']],
                include: [
                    {
                        model: Country,
                    },
                ],
            }).then(function(calculations) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got calculations!",
                    },
                    data: {
                        calculations,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get calculations!',
                },
                data: null,
            });
        });
};

/**
 * Get calculation by id
 * @param req Request data
 * @param res Response data
 */
CalculationController.getCalculation = (req, res) => {
    Promise.resolve()
        .then(function() {
            Calculation.findOne({
                where: {
                    id: req.params.id,
                },
                include: [
                    {
                        model: Country,
                    },
                ],
            }).then(function(calculation) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got calculation!",
                    },
                    data: calculation,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get calculations!',
                },
                data: null,
            });
        });
};

/**
 * Delete calc by id (admin only)
 * @param req Request data
 * @param res Response data
 */
CalculationController.deleteCalculation = (req, res) => {
    Promise.resolve()
        .then(function() {
            Calculation.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Calculation successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Calculation delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Calculation delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = CalculationController;
