'use strict';

const Block = require('../models/block');

const BlockController = {};

/**
 * Add new block (admin only)
 * @param req Request data
 * @param res Response data
 */
BlockController.addBlock = (req, res) => {
    if (!req.body.name || !req.body.description) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Block.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Block added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating blocks!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all blocks
 * @param req Request data
 * @param res Response data
 */
BlockController.getBlocks = (req, res) => {
    Promise.resolve()
        .then(() => {
            Block.findAll().then(blocks => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got blocks!",
                    },
                    data: {blocks},
                });
            });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get blocks!',
                },
                data: null,
            });
        });
};

/**
 * Get block by id
 * @param req Request data
 * @param res Response data
 */
BlockController.getBlock = (req, res) => {
    Promise.resolve()
        .then(() => {
            Block.findByPk(req.params.id).then(block => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got block!",
                    },
                    data: block,
                });
            });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get blocks!',
                },
                data: null,
            });
        });
};

/**
 * Update block by id
 * @param req Request data
 * @param res Response data
 */
BlockController.updateBlock = (req, res) => {
    Promise.resolve()
        .then(() => {
            Block.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(() => {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Block successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(e => {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Block update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(error => {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Block update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete block by id
 * @param req Request data
 * @param res Response data
 */
BlockController.deleteBlock = (req, res) => {
    Promise.resolve()
        .then(() => {
            Block.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(() => {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Block successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(e => {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Block delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Block delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = BlockController;
