'use strict';

const Application = require('../models/application');
const ApplicationType = require('../models/applicationType');
const Tour = require('../models/tour');
const mail = require('../services/mail');
const AppService = require('../services/application');

const ApplicationController = {};

/**
 * Add new application (admin only)
 * @param req Request data
 * @param res Response data
 */
ApplicationController.addApplication = async (req, res) => {
    if (
        !req.body.applicationType ||
        !req.body.name ||
        !(req.body.phone || req.body.email)
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        if (!req.body.tour) req.body.tour = -1;
        Promise.resolve()
            .then(async () => {
                return Application.create(req.body).then(async () => {
                    const appType = await AppService.getApplicationTypeById(
                        req.body.applicationType,
                    );
                    if (+req.body.applicationType === 8) {
                        mail.sendNoReplyMessage(
                            'Новая заявка на бронирование гостиницы с ВяткаТур',
                            'Имя: ' +
                            req.body.name +
                            ', Телефон: ' +
                            req.body.phone +
                            ', Гостиница: ' +
                            appType.data.country +
                            ', Ссылка на страницу гостиницы: ' +
                            appType.data.companyName,
                            'bron@vturs.ru',
                        );
                    } else if (+req.body.applicationType === 4) {
                        mail.sendNoReplyMessage(
                            'Новая заявка с ВяткаТур ' + req.body.phone,
                            'Имя: ' +
                                req.body.name +
                                ', Телефон: ' +
                                req.body.phone +
                                ', E-mail: ' +
                                req.body.email +
                                ', Тип: ' +
                                appType.data.name,
                            'bron@vturs.ru',
                        );
                    } else if (+req.body.applicationType === 5) {
                        mail.sendNoReplyMessage(
                            'Новая заявка с ВяткаТур ' + req.body.phone,
                            'Имя: ' +
                                req.body.name +
                                ', Телефон: ' +
                                req.body.phone +
                                ', E-mail: ' +
                                req.body.email +
                                ', Тип: ' +
                                appType.data.name +
                                ', Страна: ' +
                                req.body.country,
                            'visa@vturs.ru',
                        );
                    } else {
                        mail.sendNoReplyMessage(
                            'Новая заявка с ВяткаТур' + req.body.phone,
                            'Имя: ' +
                                req.body.name +
                                ', Телефон: ' +
                                req.body.phone +
                                ', E-mail: ' +
                                req.body.email +
                                ', Тип: ' +
                                appType.data.name,
                            'zapros@vturs.ru',
                        );
                    }
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Application added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating applications!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all applications
 * @param req Request data
 * @param res Response data
 */
ApplicationController.getApplications = (req, res) => {
    Promise.resolve()
        .then(function() {
            Application.findAll({
                include: [
                    {
                        model: ApplicationType,
                    },
                    {
                        model: Tour,
                    },
                ],
                order: [['id', 'DESC']],
            }).then(function(applications) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got applications!",
                    },
                    data: {
                        applications,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get applications!',
                },
                data: null,
            });
        });
};

/**
 * Get all user applications
 * @param req Request data
 * @param res Response data
 */
ApplicationController.getUserApplications = (req, res) => {
    Promise.resolve()
        .then(function() {
            Application.findAll({
                where: {
                    user: req.params.id,
                },
            }).then(function(applications) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got user applications!",
                    },
                    data: {
                        applications,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get applications!',
                },
                data: null,
            });
        });
};

/**
 * Get all tour applications
 * @param req Request data
 * @param res Response data
 */
ApplicationController.getTourApplications = (req, res) => {
    Promise.resolve()
        .then(function() {
            Application.findAll({
                where: {
                    tour: req.params.id,
                },
            }).then(function(applications) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tour applications!",
                    },
                    data: {
                        applications,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get applications!',
                },
                data: null,
            });
        });
};

/**
 * Get application by id
 * @param req Request data
 * @param res Response data
 */
ApplicationController.getApplication = (req, res) => {
    Promise.resolve()
        .then(function() {
            Application.findOne({
                where: {
                    id: req.params.id,
                },
                include: [
                    {
                        model: Tour,
                    },
                ],
            }).then(function(application) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got application!",
                    },
                    data: application,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get applications!',
                },
                data: null,
            });
        });
};

/**
 * Update application by id
 * @param req Request data
 * @param res Response data
 */
ApplicationController.updateApplication = (req, res) => {
    Promise.resolve()
        .then(function() {
            Application.update(req.body, {
                where: {
                    id: req.params.id,
                    user: req.user.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Application successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Application update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Application update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete application by id
 * @param req Request data
 * @param res Response data
 */
ApplicationController.deleteApplication = (req, res) => {
    Promise.resolve()
        .then(function() {
            Application.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Application successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Application delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Application delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = ApplicationController;
