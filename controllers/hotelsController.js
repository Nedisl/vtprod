const Hotel = require('../models/hotel');
const debug = require('debug');

const ModelName = 'Hotel';

const debugGetAll = debug(`${ModelName}-controller:get-all`);
const debugGet = debug(`${ModelName}-controller:get`);
const debugAdd = debug(`${ModelName}-controller:add`);
const debugUpdate = debug(`${ModelName}-controller:update`);
const debugDelete = debug(`${ModelName}-controller:delete`);

const requiredFields = ['name', 'pictures', 'price', 'desc'];

const Controller = {
    getAll: (req, res) => {
        try {
            Hotel.findAll().then(result => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: `${ModelName} get all result`,
                    },
                    data: result,
                });
            });
        } catch (e) {
            debugGetAll(e);
        }
    },

    get: (req, res) => {
        try {
            Hotel.findOne({
                where: {
                    id: req.params.id,
                },
            }).then(result => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: `${ModelName} get one result`,
                    },
                    data: result,
                });
            });
        } catch (e) {
            debugGet(e);
        }
    },

    add: (req, res) => {
        try {
            if (
                requiredFields
                    .map(field => req.body[field])
                    .some(reqField => reqField === null || reqField === undefined)
            ) {
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Please provide all needed fields.',
                        requiredFields,
                    },
                    data: null,
                });

                return;
            }
            Hotel.create(req.body).then(() => {
                res.status(201).json({
                    meta: {
                        success: true,
                        message: `${ModelName} added!`,
                    },
                    data: null,
                });
            });
        } catch (e) {
            debugAdd(e);
        }
    },

    update: (req, res) => {
        try {
            if (
                requiredFields
                    .map(field => req.body[field])
                    .some(reqField => reqField === null || reqField === undefined)
            ) {
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Please provide all needed fields.',
                        requiredFields,
                    },
                    data: null,
                });

                return;
            }
            Hotel.update(req.body, {
                where: {
                    id: req.params.id,
                },
            }).then(() => {
                res.status(201).json({
                    meta: {
                        success: true,
                        message: `${ModelName} updated!`,
                    },
                    data: null,
                });
            });
        } catch (e) {
            debugUpdate(e);
        }
    },

    delete: (req, res) => {
        try {
            Hotel.destroy({
                where: {
                    id: req.params.id,
                },
            }).then(() => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: `${ModelName} successfully deleted!`,
                    },
                    data: null,
                });
            });
        } catch (e) {
            debugDelete(e);
        }
    },
};

module.exports = {HotelsController: Controller};
