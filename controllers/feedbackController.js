'use strict';

const Feedback = require('../models/feedback');
const mail = require('../services/mail');
const OfficeService = require('../services/office');

const FeedbackController = {};

/**
 * Add new feedback
 * @param req Request data
 * @param res Response data
 */
FeedbackController.addFeedback = async (req, res) => {
    if (
        !req.body.comment ||
        !req.body.name ||
        !(req.body.email || req.body.phone) ||
        !req.body.office ||
        !req.body.city ||
        !req.body.type
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        const office = await OfficeService.getById(req.body.office);

        mail.sendNoReplyMessage(
            'Обратная связь',
            'E-mail: ' +
                req.body.email +
                ', Телефон: ' +
                req.body.phone +
                ', Офис: ' +
                office.title +
                ', Комментарий: ' +
                req.body.comment +
                ' Тип: ' +
                (req.body.type === 1 ? 'жалоба' : 'благодарность'),
            '043ru@mail.ru',
        );

        Promise.resolve()
            .then(() => {
                return Feedback.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Feedback added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating feedbacks!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all feedbacks
 * @param req Request data
 * @param res Response data
 */
FeedbackController.getFeedbacks = (req, res) => {
    Promise.resolve()
        .then(function() {
            Feedback.findAll().then(function(feedbacks) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got feedbacks!",
                    },
                    data: {feedbacks},
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get feedbacks!',
                },
                data: null,
            });
        });
};

/**
 * Get feedback by id
 * @param req Request data
 * @param res Response data
 */
FeedbackController.getFeedback = (req, res) => {
    Promise.resolve()
        .then(function() {
            Feedback.findByPk(req.params.id).then(function(feedback) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got feedback!",
                    },
                    data: feedback,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get feedbacks!',
                },
                data: null,
            });
        });
};

/**
 * Update feedback by id
 * @param req Request data
 * @param res Response data
 */
FeedbackController.updateFeedback = (req, res) => {
    Promise.resolve()
        .then(function() {
            Feedback.update(req.body, {
                where: {
                    id: req.params.id,
                    user: req.user.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Feedback successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Feedback update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Feedback update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete feedback by id (admin only)
 * @param req Request data
 * @param res Response data
 */
FeedbackController.deleteFeedback = (req, res) => {
    Promise.resolve()
        .then(function() {
            Feedback.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Feedback successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Feedback delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Feedback delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = FeedbackController;
