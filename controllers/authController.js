'use strict';

const jwt = require('jsonwebtoken');

const config = require('../config');

const User = require('../models/user');

const AuthController = {};

/**
 * User signup
 * @param req Request data
 * @param res Response data
 */
AuthController.signUp = (req, res) => {
    if (
        !req.body.email ||
        !req.body.password ||
        !req.body.contactName ||
        !req.body.orgName ||
        !req.body.phone
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                const newUser = {
                    orgName: req.body.orgName,
                    phone: req.body.phone,
                    email: req.body.email,
                    contactName: req.body.contactName,
                    password: req.body.password,
                    userType: 1,
                    city: 1,
                    avatar: 'images/ava.jpeg',
                };

                return User.create(newUser).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Account created!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Username already exists!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * User login
 * @param req Request data
 * @param res Response data
 */
AuthController.authenticateUser = (req, res) => {
    if (!req.body.email || !req.body.password) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        const {email} = req.body;
        const {password} = req.body;
        const potentialUser = {where: {email}};

        User.findOne(potentialUser)
            .then(user => {
                if (!user) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Auth failed!',
                        },
                        data: null,
                    });
                } else {
                    user.comparePasswords(password, (error, isMatch) => {
                        if (isMatch && !error) {
                            const token = jwt.sign(
                                {id: user.id, email: user.email, userType: user.type},
                                config.keys.secret,
                                {expiresIn: '360d'},
                            );

                            res.status(200).json({
                                meta: {
                                    success: true,
                                    message: 'User successfully logined',
                                },
                                data: {
                                    token: 'JWT ' + token,
                                    type: user.type,
                                },
                            });
                        } else {
                            console.log(error);
                            res.status(200).json({
                                meta: {
                                    success: false,
                                    message: 'Auth failed!',
                                },
                                data: null,
                            });
                        }
                    });
                }
            })
            .catch(error => {
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Auth failed!',
                    },
                    data: error,
                });
            });
    }
};

/**
 * User check is password correct
 * @param req Request data
 * @param res Response data
 */
AuthController.checkPassword = (req, res) => {
    if (!req.body.email || !req.body.password) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        const {email} = req.body;
        const {password} = req.body;
        const potentialUser = {where: {email}};

        User.findOne(potentialUser)
            .then(user => {
                if (!user) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Wrong password!',
                        },
                        data: null,
                    });
                } else {
                    user.comparePasswords(password, (error, isMatch) => {
                        if (isMatch && !error) {
                            res.status(200).json({
                                meta: {
                                    success: true,
                                    message: 'The password is ok!',
                                },
                                data: null,
                            });
                        } else {
                            res.status(200).json({
                                meta: {
                                    success: false,
                                    message: 'Wrong password!',
                                },
                                data: null,
                            });
                        }
                    });
                }
            })
            .catch(error => {
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Wrong password!',
                    },
                    data: error,
                });
            });
    }
};

module.exports = AuthController;
