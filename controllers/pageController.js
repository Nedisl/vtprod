'use strict';

const Page = require('../models/page');

const PageController = {};

/**
 * Add new page (admin only)
 * @param req Request data
 * @param res Response data
 */
PageController.addPage = (req, res) => {
    Promise.resolve()
        .then(() => {
            return Page.create(req.body).then(page => {
                res.status(201).json({
                    meta: {
                        success: true,
                        message: 'Page added!',
                    },
                    data: page,
                });
            });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Error while creating pages!',
                },
                data: null,
            });
        });
};

/**
 * Get all pages
 * @param req Request data
 * @param res Response data
 */
PageController.getPages = (req, res) => {
    Promise.resolve()
        .then(function() {
            Page.findAll().then(function(pages) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got pages!",
                    },
                    data: {pages},
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get pages!',
                },
                data: null,
            });
        });
};

/**
 * Get page by id
 * @param req Request data
 * @param res Response data
 */
PageController.getPage = (req, res) => {
    Promise.resolve()
        .then(function() {
            Page.findOne({where: {id: req.params.id}}).then(function(page) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got page!",
                    },
                    data: page,
                });
            });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get pages!',
                },
                data: null,
            });
        });
};

/**
 * Update page by id
 * @param req Request data
 * @param res Response data
 */
PageController.updatePage = (req, res) => {
    Promise.resolve()
        .then(function() {
            Page.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Page successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Page update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Page update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete page by id
 * @param req Request data
 * @param res Response data
 */
PageController.deletePage = (req, res) => {
    Promise.resolve()
        .then(function() {
            Page.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Page successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Page delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Page delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = PageController;
