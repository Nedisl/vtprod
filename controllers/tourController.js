'use strict';
const db = require('../services/database');

const Tour = require('../models/tour');
const Hotel = require('../models/hotel');
const TourPhoto = require('../models/tourPhoto');
const TourType = require('../models/tourType');
const Place = require('../models/place');
const TourHotel = require('../models/merge/tour-hotel');

const include = [
    {
        model: TourType,
    },
    {
        model: Place,
    },
    {
        model: Hotel,
    },
];

const TourController = {};

/**
 * Add new tour (admin only)
 * @param req Request data
 * @param res Response data
 */
TourController.addTour = (req, res) => {
    if (
        !req.body.name ||
        !req.body.days ||
        !req.body.includeList ||
        !req.body.notIncludeList ||
        !req.body.place ||
        !req.body.nights ||
        !req.body.tourType ||
        !req.body.pictures ||
        !req.body.costs
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Tour.create(req.body).then(tour => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Tour added!',
                        },
                        data: tour,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating tours!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all tours
 * @param req Request data
 * @param res Response data
 */
TourController.getTours = (req, res) => {
    Promise.resolve()
        .then(function() {
            Tour.findAll({
                include,
                order: [
                    ['priority', 'DESC'],
                    ['name', 'ASC'],
                ],
            }).then(function(tours) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tours!",
                    },
                    data: tours,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get tours!',
                },
                data: null,
            });
        });
};

/**
 * Get all tours
 * @param req Request data
 * @param res Response data
 */
TourController.getSelectedTours = (req, res) => {
    Promise.resolve()
        .then(function() {
            Tour.findAll({
                where: {
                    selected: 1,
                },
                include,
                order: [
                    ['priority', 'DESC'],
                    ['name', 'ASC'],
                ],
            }).then(function(tours) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tours!",
                    },
                    data: tours,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get tours!',
                },
                data: null,
            });
        });
};

/**
 * Get tour by id
 * @param req Request data
 * @param res Response data
 */
TourController.getTour = (req, res) => {
    Promise.resolve()
        .then(function() {
            Tour.findByPk(req.params.id, {include: {model: Hotel}}).then(function(tour) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tour!",
                    },
                    data: tour,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get tours!',
                },
                data: null,
            });
        });
};

/**
 * Update tour by id (admin only)
 * @param req Request data
 * @param res Response data
 */
TourController.updateTour = async (req, res) => {
    const transaction = await db.transaction();

    try {
        if (req.body.Hotels) {
            const tour = await Tour.findByPk(req.params.id);
            const hotels = await tour.getHotels();

            await tour.removeHotels(hotels);
            await Promise.all(
                req.body.Hotels.map(hotel => ({
                    HotelId: hotel.id,
                    TourId: req.params.id,
                })).map(tourHotel => TourHotel.create(tourHotel)),
            );
        }
        await Tour.update(req.body, {
            where: {
                id: req.params.id,
            },
        }).then(function() {
            res.status(200).json({
                meta: {
                    success: true,
                    message: 'Tour successfully updated!',
                },
                data: null,
            });
        });
        await transaction.commit();
    } catch (error) {
        await transaction.rollback();
        res.status(200).json({
            meta: {
                success: false,
                message: 'Tour update failed!',
            },
            data: {
                error,
            },
        });
    }
};

/**
 * Delete tour by id (admin only)
 * @param req Request data
 * @param res Response data
 */
TourController.deleteTour = (req, res) => {
    Promise.resolve()
        .then(function() {
            Tour.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Tour successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Tour delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Tour delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Add new tour photo (admin only)
 * @param req Request data
 * @param res Response data
 */
TourController.addTourPhoto = (req, res) => {
    if (!req.body.photo || !req.body.tour) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return TourPhoto.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Tour photo added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating tour photos!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all tour photos
 * @param req Request data
 * @param res Response data
 */
TourController.getTourPhotos = (req, res) => {
    Promise.resolve()
        .then(function() {
            TourPhoto.findAll({where: {tour: req.params.id}}).then(function(photos) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tour photos!",
                    },
                    data: {photos},
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get tour photos!',
                },
                data: null,
            });
        });
};

/**
 * Get photo by id
 * @param req Request data
 * @param res Response data
 */
TourController.getTourPhoto = (req, res) => {
    Promise.resolve()
        .then(function() {
            TourPhoto.findByPk(req.params.id).then(function(tour) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tour photo!",
                    },
                    data: tour,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get tour photos!',
                },
                data: null,
            });
        });
};

/**
 * Delete tour by id (admin only)
 * @param req Request data
 * @param res Response data
 */
TourController.deleteTourPhoto = (req, res) => {
    Promise.resolve()
        .then(function() {
            TourPhoto.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Tour photo successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Tour photo delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Tour photo delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Add new tour type (admin only)
 * @param req Request data
 * @param res Response data
 */
TourController.addTourType = (req, res) => {
    if (!req.body.name) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return TourType.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Tour type added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating tour types!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all tour types
 * @param req Request data
 * @param res Response data
 */
TourController.getTourTypes = (req, res) => {
    Promise.resolve()
        .then(function() {
            TourType.findAll().then(function(tourTypes) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got tour types!",
                    },
                    data: tourTypes,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get tour types!',
                },
                data: null,
            });
        });
};

/**
 * Delete tour type by id (admin only)
 * @param req Request data
 * @param res Response data
 */
TourController.deleteTourType = (req, res) => {
    Promise.resolve()
        .then(function() {
            TourType.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Tour type successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Tour type delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Tour type delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = TourController;
