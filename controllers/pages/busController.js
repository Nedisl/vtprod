'use strict';

const Reviews = require('../../services/review');
const Countries = require('../../services/country');
const Blogs = require('../../services/blog');
const Tours = require('../../services/tour');

const BusController = {};

BusController.getBusPage = async (req, res) => {
    const reviews = await Reviews.getAllPure();
    const countries = await Countries.getList();
    const blogs = await Blogs.getAll();
    const toursSelected = await Tours.getSelected();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got reviews page!",
        },
        data: {
            reviews: reviews.data,
            countries: countries.data,
            blogs: blogs.data,
            toursSelected: toursSelected.data,
        },
    });
};

module.exports = BusController;
