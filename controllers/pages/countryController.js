'use strict';

const Reviews = require('../../services/review');
const Countries = require('../../services/country');
const Months = require('../../services/month');
const Places = require('../../services/place');

const CountryController = {};

CountryController.getCountryPage = async (req, res) => {
    if (req.params.url) req.params.id = req.params.url;
    const country = await Countries.getByUrl(req.params.id);
    const countries = await Countries.getList();

    if (country.data !== null && country.data !== null && country.data !== 'null') {
        const reviews = await Reviews.getAllByCountryId(country.data.id);
        const months = await Months.getByCountry(country.data.id);
        const places = await Places.getByCode(country.data.code);

        res.status(200).json({
            meta: {
                success: true,
                message: "You've got country page!",
            },
            data: {
                reviews: reviews.data,
                countries: countries.data,
                country: country.data,
                places: places.data,
                months: months.data,
            },
        });
    } else {
        const country = await Countries.getById(req.params.id);
        const reviews = await Reviews.getAllByCountryId(country.data.id);
        const months = await Months.getByCountry(country.data.id);
        const places = await Places.getByCode(country.data.code);

        res.status(200).json({
            meta: {
                success: true,
                message: "You've got country page!",
            },
            data: {
                reviews: reviews.data,
                countries: countries.data,
                country: country.data,
                places: places.data,
                months: months.data,
            },
        });
    }
};

CountryController.getCountryPageByUrl = async (req, res) => {
    const reviews = await Reviews.getAllByCountryId(req.params.id);
    const countries = await Countries.getList();
    const country = await Countries.getByUrl(req.params.url);

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got country page!",
        },
        data: {
            reviews: reviews.data,
            countries: countries.data,
            country: country.data,
        },
    });
};

module.exports = CountryController;
