'use strict';

const Offices = require('../../services/office');
const Countries = require('../../services/country');

const OfficeController = {};

OfficeController.getContactsPages = async (req, res) => {
    const offices = await Offices.getAll();
    const countries = await Countries.getList();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got reviews page!",
        },
        data: {
            offices: offices.data,
            countries: countries.data,
        },
    });
};

module.exports = OfficeController;
