'use strict';

const Countries = require('../../services/country');
const Places = require('../../services/place');
const Months = require('../../services/month');
const Visas = require('../../services/visa');
const Reviews = require('../../services/review');

const MonthController = {
    getMonth: async (req, res) => {
        try {
            const {countryUrl, monthIndex} = req.params;

            if (!countryUrl || !monthIndex) {
                res.status(400).json({
                    meta: {
                        success: false,
                        message: 'have no url parameter',
                    },
                });

                return;
            }
            const country = (await Countries.getByUrl(countryUrl)).data;

            if (!country) {
                res.status(400).json({
                    meta: {
                        success: false,
                        message: 'have no country',
                    },
                });

                return;
            }
            const month = (await Months.getByCountryAndMonthIndex(country.id, monthIndex))
                .data;

            if (!month) {
                res.status(400).json({
                    meta: {
                        success: false,
                        message: 'have no month',
                    },
                });

                return;
            }
            const [
                monthsRes,
                placesRes,
                countriesRes,
                visa,
                reviews,
            ] = await Promise.all([
                Months.getByCountry(month.country, ['month']),
                Places.getByCode(country.code, [
                    'id',
                    'url',
                    'previewPhoto',
                    'name',
                    'price',
                ]),
                Countries.getList(['id', 'name', 'url']),
                Visas.getOneByCountryId(country.id, ['id', 'url']),
                Reviews.getAllByCountryId(country.id, false, [
                    'pictures',
                    'name',
                    'country',
                    'rating',
                    'date',
                    'comment',
                    'onlyAva',
                ]),
            ]);

            res.status(200).json({
                meta: {
                    success: true,
                    message: "You've got month page!",
                },
                data: {
                    country,
                    places: placesRes.data,
                    months: monthsRes.data,
                    countries: countriesRes.data,
                    reviews: reviews.data,
                    month,
                    visa: visa.data,
                },
            });
        } catch (e) {
            res.status(400).json({
                meta: {
                    success: false,
                    message: 'server error',
                },
            });
            console.log('error: ', e);
        }
    },
};

module.exports = MonthController;
