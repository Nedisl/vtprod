'use strict';

const Reviews = require('../../services/review');
const Countries = require('../../services/country');
const Blogs = require('../../services/blog');
const Slides = require('../../services/slide');
const Offices = require('../../services/office');
const Visas = require('../../services/visa');
const Tours = require('../../services/tour');

const IndexController = {};

IndexController.getIndexPage = async (req, res) => {
    const reviews = await Reviews.getAllPure();
    const countries = await Countries.getList();
    const countriesPopular = await Countries.getPopularList();
    const countriesSelected = await Countries.getSelected();
    const offices = await Offices.getAll();
    const slides = await Slides.getAll();
    const blogs = await Blogs.getAll();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got bonuses page!",
        },
        data: {
            reviews: reviews.data,
            countries: countries.data,
            countriesPopular: countriesPopular.data,
            countriesSelected: countriesSelected.data,
            offices: offices.data,
            slides: slides.data,
            blogs: blogs.data,
        },
    });
};

IndexController.getSiteMap = async (req, res) => {
    const countries = await Countries.getList();
    const visas = await Visas.getAll();
    const tours = await Tours.getAll();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got bonuses page!",
        },
        data: {
            countries: countries.data,
            visas: visas.data,
            tours: tours.data,
        },
    });
};

module.exports = IndexController;
