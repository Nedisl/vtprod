'use strict';

const Countries = require('../../services/country');
const Offices = require('../../services/office');
const Pages = require('../../services/page');
const Tours = require('../../services/tour');

const InitController = {};

InitController.getInitPage = async (req, res) => {
    const countries = await Countries.getList();
    const banner = await Tours.getBanner();
    const offices = await Offices.getAll();
    const pages = await Pages.getAll();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got bonuses page!",
        },
        data: {
            countries: countries.data,
            offices: offices.data,
            pages: pages.data,
            tour: banner.data,
        },
    });
};

module.exports = InitController;
