'use strict';

const Countries = require('../../services/country');
const Places = require('../../services/place');
const Months = require('../../services/month');
const Visas = require('../../services/visa');
const Reviews = require('../../services/review');
const PlaceController = {};

PlaceController.getPlace = async (req, res) => {
    const place = (await Places.getByUrl(req.params.url)).data;
    const country = (await Countries.getByCode(place.countryCode)).data;

    const [monthsRes, placesRes, countriesRes, visa, reviews] = await Promise.all([
        Months.getByCountry(country.id, ['month']),
        Places.getByCode(country.code, ['id', 'url', 'previewPhoto', 'name', 'price']),
        Countries.getList(['id', 'name', 'url']),
        Visas.getOneByCountryId(country.id, ['id', 'url']),
        Reviews.getAllByCountryId(country.id, false, [
            'pictures',
            'name',
            'country',
            'rating',
            'date',
            'comment',
            'onlyAva',
        ]),
    ]);

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got place page!",
        },
        data: {
            place,
            country,
            months: monthsRes.data,
            places: placesRes.data,
            countries: countriesRes.data,
            reviews: reviews.data,
            visa: visa.data,
        },
    });
};

module.exports = PlaceController;
