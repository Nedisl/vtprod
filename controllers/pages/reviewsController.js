'use strict';

const Reviews = require('../../services/review');
const Countries = require('../../services/country');

const ReviewController = {};

ReviewController.getReviewsPage = async (req, res) => {
    const reviews = await Reviews.getAllPure();
    const countries = await Countries.getList();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got reviews page!",
        },
        data: {
            reviews: reviews.data,
            countries: countries.data,
        },
    });
};

module.exports = ReviewController;
