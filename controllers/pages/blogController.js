'use strict';

const Blogs = require('../../services/blog');
const Countries = require('../../services/country');

const BlogController = {};

BlogController.getBlogPage = async (req, res) => {
    const countries = await Countries.getList();
    const blogs = await Blogs.getAll();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got blog page!",
        },
        data: {
            countries: countries.data,
            blog: blogs.data,
        },
    });
};

module.exports = BlogController;
