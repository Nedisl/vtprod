'use strict';

const Tours = require('../../services/tour');

const TourController = {};

TourController.getTourPage = async (req, res) => {
    const tour = await Tours.getByUrl(req.params.id);

    if (tour.data !== null && tour.data !== null && tour.data !== 'null') {
        res.status(200).json({
            meta: {
                success: true,
                message: "You've got tour page!",
            },
            data: {
                tour: tour.data,
            },
        });
    } else {
        const tour = await Tours.getById(req.params.id);

        res.status(200).json({
            meta: {
                success: true,
                message: "You've got tour page!",
            },
            data: {
                tour: tour.data,
            },
        });
    }
};

TourController.getTourPageUrl = async (req, res) => {
    const tour = await Tours.getByUrl(req.params.url);

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got tour page!",
        },
        data: {
            tour: tour.data,
        },
    });
};

module.exports = TourController;
