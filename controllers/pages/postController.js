'use strict';

const Blogs = require('../../services/blog');
const Countries = require('../../services/country');

const PostController = {};

PostController.getPostPage = async (req, res) => {
    const countries = await Countries.getList();
    const post = await Blogs.getByUrl(req.params.id);
    const randomPosts = await Blogs.getRandomBlogs(req.params.id);

    if (post.data !== null && post.data !== null && post.data !== 'null') {
        res.status(200).json({
            meta: {
                success: true,
                message: "You've got blog page!",
            },
            data: {
                countries: countries.data,
                post: post.data,
                blogAlsoRead: randomPosts.data,
            },
        });
    } else {
        const post = await Blogs.getById(req.params.id);

        res.status(200).json({
            meta: {
                success: true,
                message: "You've got blog page!",
            },
            data: {
                countries: countries.data,
                post: post.data,
                blogAlsoRead: randomPosts.data,
            },
        });
    }
};

module.exports = PostController;
