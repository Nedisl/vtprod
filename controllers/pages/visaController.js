'use strict';

const Visas = require('../../services/visa');
const Countries = require('../../services/country');

const VisaController = {};

VisaController.getVisaPage = async (req, res) => {
    const visa = await Visas.getByUrl(req.params.id);
    const countries = await Countries.getList();

    if (visa.data !== null && visa.data !== null && visa.data !== 'null') {
        res.status(200).json({
            meta: {
                success: true,
                message: "You've got visa page!",
            },
            data: {
                visa: visa.data,
                countries: countries.data,
            },
        });
    } else {
        const visa = await Visas.getById(req.params.id);

        res.status(200).json({
            meta: {
                success: true,
                message: "You've got visa page!",
            },
            data: {
                visa: visa.data,
                countries: countries.data,
            },
        });
    }
};

VisaController.getVisaPageByUrl = async (req, res) => {
    const visa = await Visas.getByUrl(req.params.url);
    const countries = await Countries.getList();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got visa page!",
        },
        data: {
            visa: visa.data,
            countries: countries.data,
        },
    });
};

module.exports = VisaController;
