'use strict';

const Teams = require('../../services/team');
const Office = require('../../services/office');
const Countries = require('../../services/country');

const AboutController = {};

AboutController.getAboutPage = async (req, res) => {
    const countries = await Countries.getList();
    const teams = await Teams.getAll();
    const countOffices = await Office.getCount();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got about page!",
        },
        data: {
            countries: countries.data,
            team: teams.data,
            officesCount: countOffices.data,
        },
    });
};

module.exports = AboutController;
