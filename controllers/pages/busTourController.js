'use strict';

const Reviews = require('../../services/review');
const Countries = require('../../services/country');
const Tours = require('../../services/tour');
const BusTours = require('../../services/busTour');

const BusTourController = {};

BusTourController.getBusPage = async (req, res) => {
    const reviews = await Reviews.getAllPure();
    const countries = await Countries.getList();
    const busTour = await BusTours.getTour();
    const toursBusSelected = await Tours.getBusSelected();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got reviews page!",
        },
        data: {
            reviews: reviews.data,
            countries: countries.data,
            busTour: busTour.data,
            toursBusSelected: toursBusSelected.data,
        },
    });
};

module.exports = BusTourController;
