'use strict';

const Countries = require('../../services/country');

const InstalmentsController = {};

InstalmentsController.getInstalmentsPage = async (req, res) => {
    const countries = await Countries.getList();
    const countriesPopular = await Countries.getPopularList();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got instalments page!",
        },
        data: {
            countries: countries.data,
            countriesPopular: countriesPopular.data,
        },
    });
};

module.exports = InstalmentsController;
