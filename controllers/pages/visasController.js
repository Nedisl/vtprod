'use strict';

const Visas = require('../../services/visa');
const Reviews = require('../../services/review');
const Countries = require('../../services/country');

const VisasController = {};

VisasController.getVisasPage = async (req, res) => {
    const visas = await Visas.getAll();
    const visasPopular = await Visas.getPopular();
    const reviews = await Reviews.getAllPure();
    const countries = await Countries.getList();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got visas page!",
        },
        data: {
            visas: visas.data,
            visasPopular: visasPopular.data,
            reviews: reviews.data,
            countries: countries.data,
        },
    });
};

module.exports = VisasController;
