'use strict';

const Countries = require('../../services/country');

const BonusesController = {};

BonusesController.getBonusesPage = async (req, res) => {
    const countries = await Countries.getList();

    res.status(200).json({
        meta: {
            success: true,
            message: "You've got bonuses page!",
        },
        data: {
            countries: countries.data,
        },
    });
};

module.exports = BonusesController;
