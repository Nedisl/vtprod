'use strict';

const Icon = require('../models/icon');

const IconController = {};

/**
 * Add new icon
 * @param req Request data
 * @param res Response data
 */
IconController.addIcon = (req, res) => {
    if (!req.body.name || !req.body.path) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Icon.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Icon added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating icons!',
                    },
                    data: error,
                });
            });
    }
};

/**
 * Get all icons
 * @param req Request data
 * @param res Response data
 */
IconController.getIcons = (req, res) => {
    Promise.resolve()
        .then(function() {
            Icon.findAll().then(function(icons) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got icons!",
                    },
                    data: {icons},
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get countries!',
                },
                data: null,
            });
        });
};

/**
 * Delete icon by id (admin only)
 * @param req Request data
 * @param res Response data
 */
IconController.deleteIcon = (req, res) => {
    Promise.resolve()
        .then(function() {
            Icon.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Icon successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Icon delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Icon delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = IconController;
