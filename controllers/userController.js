'use strict';

const User = require('../models/user');

const UserController = {};

/**
 * Get user profile
 * @param req Request data
 * @param res Response data
 */
UserController.getProfile = (req, res) => {
    console.log(req.user.id);

    if (!req.user.id) {
        res.json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(function() {
                User.findOne({
                    where: {
                        id: req.user.id,
                    },
                }).then(function(user) {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'You got user info',
                        },
                        data: {
                            user,
                        },
                    });
                });
            })
            .catch(function(error) {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error',
                    },
                    data: error,
                });
            });
    }
};

module.exports = UserController;
