'use strict';

const Subscriber = require('../models/subscribers');

const SubscriberController = {};

/**
 * Add new subscriber
 * @param req Request data
 * @param res Response data
 */
SubscriberController.addSubscriber = (req, res) => {
    if (!req.body.email) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Subscriber.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Subscriber added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating applications!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all subscribers
 * @param req Request data
 * @param res Response data
 */
SubscriberController.getSubscribers = (req, res) => {
    Promise.resolve()
        .then(function() {
            Subscriber.findAll().then(function(subscribers) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got subscribers!",
                    },
                    data: {subscribers},
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get subscribers!',
                },
                data: null,
            });
        });
};

module.exports = SubscriberController;
