'use strict';

const City = require('../models/city');

const CityController = {};

/**
 * Add new city (admin only)
 * @param req Request data
 * @param res Response data
 */
CityController.addCity = (req, res) => {
    if (!req.body.name) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return City.create(req.body).then(city => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'City added!',
                        },
                        data: city,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating cities!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all citys
 * @param req Request data
 * @param res Response data
 */
CityController.getCities = (req, res) => {
    Promise.resolve()
        .then(() => {
            City.findAll().then(cities => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got cities!",
                    },
                    data: {cities},
                });
            });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get cities!',
                },
                data: null,
            });
        });
};

/**
 * Get city by id
 * @param req Request data
 * @param res Response data
 */
CityController.getCity = (req, res) => {
    Promise.resolve()
        .then(() => {
            City.findByPk(req.params.id).then(city => {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got city!",
                    },
                    data: city,
                });
            });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get city!',
                },
                data: null,
            });
        });
};

/**
 * Update city by id
 * @param req Request data
 * @param res Response data
 */
CityController.updateCity = (req, res) => {
    Promise.resolve()
        .then(() => {
            City.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(() => {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'City successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(e => {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'City update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(error => {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'City update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete city by id
 * @param req Request data
 * @param res Response data
 */
CityController.deleteCity = (req, res) => {
    Promise.resolve()
        .then(() => {
            City.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(() => {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'City successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(e => {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'City delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(error => {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'City delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = CityController;
