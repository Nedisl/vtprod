'use strict';

const Country = require('../models/country');
const Quote = require('../models/quote');
const Team = require('../models/team');
const CountryPhoto = require('../models/countryPhoto');

const CountryController = {};

/**
 * Add new country (admin only)
 * @param req Request data
 * @param res Response data
 */
CountryController.addCountry = (req, res) => {
    if (
        !req.body.name ||
        !req.body.lat ||
        !req.body.lng ||
        !req.body.flagPicture ||
        !req.body.background ||
        !req.body.guide ||
        !req.body.startPrice ||
        !req.body.hasOwnProperty('isPopular') ||
        !req.body.hasOwnProperty('available')
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Country.create(req.body).then(country => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Country added!',
                        },
                        data: country,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating countries!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all countries
 * @param req Request data
 * @param res Response data
 */
CountryController.getCountries = (req, res) => {
    Promise.resolve()
        .then(function() {
            Country.findAll({
                where: {
                    available: 1,
                },
                order: [['name', 'ASC']],
            }).then(function(countries) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got countries!",
                    },
                    data: {
                        countries,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get countries!',
                },
                data: null,
            });
        });
};

/**
 * Get all admin countries
 * @param req Request data
 * @param res Response data
 */
CountryController.getAdminCountries = (req, res) => {
    Promise.resolve()
        .then(function() {
            Country.findAll().then(function(countries) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got countries!",
                    },
                    data: {
                        countries,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get countries!',
                },
                data: null,
            });
        });
};

/**
 * Get all countries
 * @param req Request data
 * @param res Response data
 */
CountryController.getPopularCountries = (req, res) => {
    Promise.resolve()
        .then(function() {
            Country.findAll({
                where: {
                    isPopular: 1,
                },
                limit: 6,
            }).then(function(countries) {
                console.log(countries);
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got countries!",
                    },
                    data: {
                        countries,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get countries!',
                },
                data: null,
            });
        });
};

/**
 * Get all countries
 * @param req Request data
 * @param res Response data
 */
CountryController.getSelectedCountries = (req, res) => {
    Promise.resolve()
        .then(function() {
            Country.findAll({
                where: {
                    selected: 1,
                },
            }).then(function(countries) {
                console.log(countries);
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got countries!",
                    },
                    data: {
                        countries,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get countries!',
                },
                data: null,
            });
        });
};

/**
 * Get country by id
 * @param req Request data
 * @param res Response data
 */
CountryController.getCountry = (req, res) => {
    Promise.resolve()
        .then(function() {
            Country.findOne({
                where: {
                    id: req.params.id,
                    available: 1,
                },
                include: [
                    {
                        model: Quote,
                        include: [Team],
                    },
                ],
            }).then(function(country) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got country!",
                    },
                    data: country,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get countries!',
                },
                data: null,
            });
        });
};
/**
 * Get country by id
 * @param req Request data
 * @param res Response data
 */
CountryController.getAdminCountry = (req, res) => {
    Promise.resolve()
        .then(function() {
            Country.findOne({
                where: {
                    id: req.params.id,
                },
                include: [
                    {
                        model: Quote,
                        include: [Team],
                    },
                ],
            }).then(function(country) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got country!",
                    },
                    data: country,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get countries!',
                },
                data: null,
            });
        });
};

/**
 * Update country by id (admin only)
 * @param req Request data
 * @param res Response data
 */
CountryController.updateCountry = (req, res) => {
    Promise.resolve()
        .then(function() {
            Country.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Country successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Country update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Country update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete country by id (admin only)
 * @param req Request data
 * @param res Response data
 */
CountryController.deleteCountry = (req, res) => {
    Promise.resolve()
        .then(function() {
            Country.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Country successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Country delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Country delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Add new country photo (admin only)
 * @param req Request data
 * @param res Response data
 */
CountryController.addCountryPhoto = (req, res) => {
    if (!req.body.picture || !req.body.country) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return CountryPhoto.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Country photo added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating country photos!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all country photos
 * @param req Request data
 * @param res Response data
 */
CountryController.getCountryPhotos = (req, res) => {
    Promise.resolve()
        .then(function() {
            CountryPhoto.findAll({
                where: {
                    country: req.params.id,
                },
            }).then(function(photos) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got country photos!",
                    },
                    data: {
                        photos,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get country photos!',
                },
                data: null,
            });
        });
};

/**
 * Get photo by id
 * @param req Request data
 * @param res Response data
 */
CountryController.getCountryPhoto = (req, res) => {
    Promise.resolve()
        .then(function() {
            CountryPhoto.findByPk(req.params.id).then(function(country) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got country photo!",
                    },
                    data: country,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get country photos!',
                },
                data: null,
            });
        });
};

/**
 * Delete country by id (admin only)
 * @param req Request data
 * @param res Response data
 */
CountryController.deleteCountryPhoto = (req, res) => {
    Promise.resolve()
        .then(function() {
            CountryPhoto.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Country photo successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Country photo delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Country photo delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = CountryController;
