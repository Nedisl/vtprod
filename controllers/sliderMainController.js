'use strict';

const sliderMain = require('../models/sliderMain');

const SliderMainController = {};

/**
 * Add new slide
 * @param req Request data
 * @param res Response data
 */
SliderMainController.addSlide = (req, res) => {
    if (
        !req.body.title ||
        !req.body.country ||
        !req.body.description ||
        !req.body.picture
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return sliderMain.create(req.body).then(slide => {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Slide added!',
                        },
                        data: slide,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating slide!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all slides
 * @param req Request data
 * @param res Response data
 */
SliderMainController.getSlides = (req, res) => {
    Promise.resolve()
        .then(function() {
            sliderMain.findAll().then(function(slides) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got slides!",
                    },
                    data: {
                        slides,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get slides!',
                },
                data: null,
            });
        });
};

/**
 * Get slide
 * @param req Request data
 * @param res Response data
 */
SliderMainController.getSlide = (req, res) => {
    Promise.resolve()
        .then(function() {
            sliderMain
                .findOne({
                    where: {
                        id: req.params.id,
                    },
                })
                .then(function(slide) {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: "You've got slide!",
                        },
                        data: slide,
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get slide!',
                },
                data: null,
            });
        });
};

SliderMainController.deleteSlide = (req, res) => {
    Promise.resolve()
        .then(function() {
            sliderMain
                .destroy({
                    where: {
                        id: req.params.id,
                    },
                })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Slide successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Slide delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Slide delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Update slider by id (admin only)
 * @param req Request data
 * @param res Response data
 */
SliderMainController.updateSlider = (req, res) => {
    Promise.resolve()
        .then(function() {
            sliderMain
                .update(req.body, {
                    where: {
                        id: req.params.id,
                    },
                })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Slider successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Slider update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Slider update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = SliderMainController;
