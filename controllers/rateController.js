'use strict';

const Rate = require('../models/rate');
const mail = require('../services/mail');

const RateController = {};

/**
 * Add new rate
 * @param req Request data
 * @param res Response data
 */
RateController.addRate = (req, res) => {
    if (!req.body.comment || !req.body.param1 || !req.body.param2 || !req.body.param3) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        mail.sendNoReplyMessage(
            'Оценка ВяткаТур',
            'Скорость загрузки страниц: ' +
                req.body.param1 +
                ', Полнота информации: ' +
                req.body.param2 +
                ', Удобство использования: ' +
                req.body.param3 +
                ', Комментарий: ' +
                req.body.comment,
            'kovtunenko.oksana@gmail.com',
        );

        Promise.resolve()
            .then(() => {
                return Rate.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Rate added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating rates!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all rates
 * @param req Request data
 * @param res Response data
 */
RateController.getRates = (req, res) => {
    Promise.resolve()
        .then(function() {
            Rate.findAll().then(function(rates) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got rates!",
                    },
                    data: {rates},
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get rates!',
                },
                data: null,
            });
        });
};

/**
 * Get rate by id
 * @param req Request data
 * @param res Response data
 */
RateController.getRate = (req, res) => {
    Promise.resolve()
        .then(function() {
            Rate.findByPk(req.params.id).then(function(rate) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got rate!",
                    },
                    data: rate,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get rates!',
                },
                data: null,
            });
        });
};

/**
 * Update rate by id
 * @param req Request data
 * @param res Response data
 */
RateController.updateRate = (req, res) => {
    Promise.resolve()
        .then(function() {
            Rate.update(req.body, {
                where: {
                    id: req.params.id,
                    user: req.user.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Rate successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Rate update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Rate update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete rate by id (admin only)
 * @param req Request data
 * @param res Response data
 */
RateController.deleteRate = (req, res) => {
    Promise.resolve()
        .then(function() {
            Rate.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Rate successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Rate delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Rate delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = RateController;
