'use strict';

const Visa = require('../models/visa');
const VisaPhoto = require('../models/visaPhoto');
const Country = require('../models/country');

const VisaController = {};

/**
 * Add new visa (admin only)
 * @param req Request data
 * @param res Response data
 */
VisaController.addVisa = (req, res) => {
    if (
        !req.body.title ||
        !req.body.content ||
        !req.body.country ||
        !req.body.price ||
        !req.body.days
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Visa.create(req.body).then(visa => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Visa added!',
                        },
                        data: visa,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating visas!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Add new visa photo (admin only)
 * @param req Request data
 * @param res Response data
 */
VisaController.addVisaPhoto = (req, res) => {
    if (!req.body.visa || !req.body.picture) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return VisaPhoto.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Visa photo added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating visa photos!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all visas
 * @param req Request data
 * @param res Response data
 */
VisaController.getVisas = (req, res) => {
    Promise.resolve()
        .then(function() {
            Visa.findAll({
                include: [
                    {
                        model: Country,
                    },
                ],
                order: [['title', 'ASC']],
            }).then(function(visas) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got visas!",
                    },
                    data: {
                        visas,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get visas!',
                },
                data: null,
            });
        });
};

/**
 * Get popular visas
 * @param req Request data
 * @param res Response data
 */
VisaController.getPopularVisas = (req, res) => {
    Promise.resolve()
        .then(function() {
            Visa.findAll({
                include: [
                    {
                        model: Country,
                    },
                ],
                where: {
                    isPopular: 1,
                },
            }).then(function(visas) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got visas!",
                    },
                    data: {
                        visas,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get visas!',
                },
                data: null,
            });
        });
};

/**
 * Get all visa photos
 * @param req Request data
 * @param res Response data
 */
VisaController.getVisaPhotos = (req, res) => {
    Promise.resolve()
        .then(function() {
            VisaPhoto.findAll({
                where: {
                    visa: req.params.id,
                },
            }).then(function(visas) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got visas!",
                    },
                    data: {
                        visas,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get visas!',
                },
                data: null,
            });
        });
};

/**
 * Get visa by id
 * @param req Request data
 * @param res Response data
 */
VisaController.getVisa = (req, res) => {
    Promise.resolve()
        .then(function() {
            Visa.findOne({
                include: [
                    {
                        model: Country,
                    },
                ],
                where: {
                    id: req.params.id,
                },
            }).then(function(visa) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got visa!",
                    },
                    data: visa,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get visas!',
                },
                data: null,
            });
        });
};

/**
 * Get visa by id
 * @param req Request data
 * @param res Response data
 */
VisaController.getVisaByCountry = (req, res) => {
    Promise.resolve()
        .then(function() {
            Visa.findOne({
                where: {
                    country: req.params.id,
                },
            }).then(function(visa) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got visa!",
                    },
                    data: visa,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get visas!',
                },
                data: null,
            });
        });
};

/**
 * Update visa by id
 * @param req Request data
 * @param res Response data
 */
VisaController.updateVisa = (req, res) => {
    Promise.resolve()
        .then(function() {
            Visa.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Visa successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Visa update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Visa update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete visa by id
 * @param req Request data
 * @param res Response data
 */
VisaController.deleteVisa = (req, res) => {
    Promise.resolve()
        .then(function() {
            Visa.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Visa successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Visa delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Visa delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete visa by id
 * @param req Request data
 * @param res Response data
 */
VisaController.deleteVisaPhoto = (req, res) => {
    Promise.resolve()
        .then(function() {
            VisaPhoto.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Visa photo successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Visa photo delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Visa photo delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = VisaController;
