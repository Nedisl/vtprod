'use strict';

const Country = require('../models/country');
const Month = require('../models/month');

const MonthController = {
    /**
     * Add new Month (admin only)
     * @param req Request data
     * @param res Response data
     */
    //todo реализовать проверку на существование пары страна-месяц
    addMonth: async (req, res) => {
        if (!req.body.month || !req.body.description || !req.body.country) {
            res.status(400).json({
                meta: {
                    success: false,
                    message: 'Please provide all needed fields.',
                },
                data: null,
            });
        } else {
            const exist = await Month.findOne({
                where: {
                    country: req.body.country,
                    month: req.body.month,
                },
            });

            if (exist) {
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'already exist',
                    },
                    data: null,
                });

                return;
            }
            Promise.resolve()
                .then(() => {
                    return Month.create(req.body).then(month => {
                        res.status(201).json({
                            meta: {
                                success: true,
                                message: 'Month added!',
                            },
                            data: month,
                        });
                    });
                })
                .catch(error => {
                    console.log(error);
                    res.status(403).json({
                        meta: {
                            success: false,
                            message: 'Error while creating months!',
                        },
                        data: null,
                    });
                });
        }
    },

    /**
     * Get all months
     * @param req Request data
     * @param res Response data
     */
    getMonths: (req, res) => {
        Promise.resolve()
            .then(function() {
                Month.findAll({
                    order: [['month', 'ASC']],
                    include: [
                        {
                            model: Country,
                        },
                    ],
                }).then(function(months) {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: "You've got months!",
                        },
                        data: months,
                    });
                });
            })
            .catch(function(error) {
                console.log(error);
                res.status(403).json({
                    meta: {
                        success: false,
                        message: 'Can not get months!',
                    },
                    data: null,
                });
            });
    },

    /**
     * Get months by month number
     * @param req Request data
     * @param res Response data
     */
    getMonthsByNumber: (req, res) => {
        Promise.resolve()
            .then(function() {
                Month.findAll({
                    where: {
                        month: req.body.month,
                    },
                    include: [
                        {
                            model: Country,
                        },
                    ],
                }).then(function(months) {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: "You've got months!",
                        },
                        data: months,
                    });
                });
            })
            .catch(function(error) {
                console.log(error);
                res.status(403).json({
                    meta: {
                        success: false,
                        message: 'Can not get months!',
                    },
                    data: null,
                });
            });
    },

    /**
     * Get month by id
     * @param req Request data
     * @param res Response data
     */
    getMonth: (req, res) => {
        Promise.resolve()
            .then(function() {
                Month.findOne({
                    where: {
                        month: req.body.month,
                    },
                    include: [
                        {
                            model: Country,
                        },
                    ],
                }).then(function(month) {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: "You've got month!",
                        },
                        data: month,
                    });
                });
            })
            .catch(function(error) {
                console.log(error);
                res.status(403).json({
                    meta: {
                        success: false,
                        message: 'Can not get months!',
                    },
                    data: null,
                });
            });
    },

    /**
     * Get month by month and country
     * @param req Request data
     * @param res Response data
     */
    getMonthsByNumberAndCountry: (req, res) => {
        Promise.resolve()
            .then(function() {
                if (!req.query.country) {
                    req.query.country = 1;
                }
                Month.findOne({
                    where: {
                        month: req.params.month,
                        country: req.query.country,
                    },
                    include: [
                        {
                            model: Country,
                        },
                    ],
                }).then(function(months) {
                    if (!months) {
                        res.status(200).json({
                            meta: {
                                success: false,
                                message: 'Have no month',
                            },
                            data: null,
                        });
                    }
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: "You've got months!",
                        },
                        data: months,
                    });
                });
            })
            .catch(function(error) {
                console.log(error);
                res.status(403).json({
                    meta: {
                        success: false,
                        message: 'Can not get months!',
                    },
                    data: null,
                });
            });
    },

    /**
     * Get month by country
     * @param req Request data
     * @param res Response data
     */
    getMonthsByCountry: (req, res) => {
        Promise.resolve()
            .then(function() {
                Month.findAll({
                    where: {
                        country: req.params.country,
                    },
                    include: [
                        {
                            model: Country,
                        },
                    ],
                }).then(function(months) {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: "You've got months!",
                        },
                        data: months,
                    });
                });
            })
            .catch(function(error) {
                console.log(error);
                res.status(403).json({
                    meta: {
                        success: false,
                        message: 'Can not get months!',
                    },
                    data: null,
                });
            });
    },

    /**
     * Update Month by id (admin only)
     * @param req Request data
     * @param res Response data
     */
    //todo реализовать проверку на существование пары страна-месяц, если изменмился месяц
    updateMonth: (req, res) => {
        Promise.resolve()
            .then(function() {
                const {
                    description,
                    scriptCode,
                    photo,
                    descriptionPhoto,
                    meta_title,
                    isOutput,
                    meta_description,
                    meta_keywords,
                    header,
                } = req.body;

                Month.update(
                    {
                        description,
                        scriptCode,
                        photo,
                        descriptionPhoto,
                        meta_title,
                        isOutput,
                        meta_description,
                        meta_keywords,
                        header,
                    },
                    {
                        where: {
                            id: req.params.id,
                        },
                    },
                )
                    .then(function() {
                        res.status(200).json({
                            meta: {
                                success: true,
                                message: 'Month successfully updated!',
                            },
                            data: null,
                        });
                    })
                    .catch(function(e) {
                        res.status(403).json({
                            meta: {
                                success: false,
                                message: 'Month update failed!',
                            },
                            data: {
                                error: e,
                            },
                        });
                    });
            })
            .catch(function(error) {
                res.status(403).json({
                    meta: {
                        success: false,
                        message: 'Month update failed!',
                    },
                    data: {
                        error,
                    },
                });
            });
    },

    /**
     * Delete Month by id (admin only)
     * @param req Request data
     * @param res Response data
     */
    deleteMonth: (req, res) => {
        Promise.resolve()
            .then(function() {
                Month.destroy({
                    where: {
                        id: req.params.id,
                    },
                })
                    .then(function() {
                        res.status(200).json({
                            meta: {
                                success: true,
                                message: 'Month successfully deleted!',
                            },
                            data: null,
                        });
                    })
                    .catch(function(e) {
                        res.status(403).json({
                            meta: {
                                success: false,
                                message: 'Month delete failed!',
                            },
                            data: {
                                error: e,
                            },
                        });
                    });
            })
            .catch(function(error) {
                console.log(error);
                res.status(403).json({
                    meta: {
                        success: false,
                        message: 'Month delete failed!',
                    },
                    data: {
                        error,
                    },
                });
            });
    },
};

module.exports = MonthController;
