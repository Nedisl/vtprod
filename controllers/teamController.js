'use strict';

const Team = require('../models/team');
const Office = require('../models/office');

const TeamController = {};

/**
 * Add new team
 * @param req Request data
 * @param res Response data
 */
TeamController.addTeam = async (req, res) => {
    if (
        !req.body.picture ||
        !req.body.name ||
        !req.body.post ||
        !req.body.phone ||
        !req.body.office
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        const teamCount = await Team.count();

        req.body.position = teamCount + 1;
        Promise.resolve()
            .then(() => {
                return Team.create(req.body).then(team => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Team added!',
                        },
                        data: team,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating teams!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all teams
 * @param req Request data
 * @param res Response data
 */
TeamController.getTeam = (req, res) => {
    Promise.resolve()
        .then(function() {
            Team.findAll({
                include: [
                    {
                        model: Office,
                    },
                ],
                order: [['position', 'ASC']],
            }).then(function(team) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got team!",
                    },
                    data: {
                        team,
                    },
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get team!',
                },
                data: null,
            });
        });
};

/**
 * Get team by id
 * @param req Request data
 * @param res Response data
 */
TeamController.getTeamMember = (req, res) => {
    Promise.resolve()
        .then(function() {
            Team.findByPk(req.params.id).then(function(member) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got team!",
                    },
                    data: member,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get teams!',
                },
                data: null,
            });
        });
};

/**
 * Update team by id
 * @param req Request data
 * @param res Response data
 */
TeamController.updateTeamMember = (req, res) => {
    Promise.resolve()
        .then(function() {
            Team.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Team member successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Team member update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Team member update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete team by id (admin only)
 * @param req Request data
 * @param res Response data
 */
TeamController.deleteTeamMember = (req, res) => {
    Promise.resolve()
        .then(function() {
            Team.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Team member successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Team member delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Team member delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = TeamController;
