'use strict';

const Office = require('../models/office');
const City = require('../models/city');

const OfficeController = {};

/**
 * Add new office (admin only)
 * @param req Request data
 * @param res Response data
 */
OfficeController.addOffice = (req, res) => {
    if (
        !req.body.title ||
        !req.body.info ||
        !req.body.busStop ||
        !req.body.phone ||
        !req.body.mail ||
        !req.body.schedule ||
        !req.body.address ||
        !req.body.picture ||
        !req.body.city
    ) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Office.create(req.body).then(office => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Office added!',
                        },
                        data: office,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating offices!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all offices
 * @param req Request data
 * @param res Response data
 */
OfficeController.getOffices = (req, res) => {
    Promise.resolve()
        .then(function() {
            Office.findAll({
                include: [
                    {
                        model: City,
                    },
                ],
            }).then(function(offices) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got offices!",
                    },
                    data: {offices},
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get offices!',
                },
                data: null,
            });
        });
};

/**
 * Get office by id
 * @param req Request data
 * @param res Response data
 */
OfficeController.getOffice = (req, res) => {
    Promise.resolve()
        .then(function() {
            Office.findOne({
                where: {id: req.params.id},
                include: [
                    {
                        model: City,
                    },
                ],
            }).then(function(office) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got office!",
                    },
                    data: office,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get offices!',
                },
                data: null,
            });
        });
};

/**
 * Update office by id
 * @param req Request data
 * @param res Response data
 */
OfficeController.updateOffice = (req, res) => {
    Promise.resolve()
        .then(function() {
            Office.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Office successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Office update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Office update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete office by id
 * @param req Request data
 * @param res Response data
 */
OfficeController.deleteOffice = (req, res) => {
    Promise.resolve()
        .then(function() {
            Office.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Office successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Office delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Office delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = OfficeController;
