'use strict';

const Place = require('../models/place');

const PlaceController = {};

/**
 * Add new place (admin only)
 * @param req Request data
 * @param res Response data
 */
PlaceController.addPlace = (req, res) => {
    if (!req.body.name || !req.body.lat || !req.body.lng || !req.body.countryCode) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                return Place.create(req.body).then(place => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Place added!',
                        },
                        data: place,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating places!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all places
 * @param req Request data
 * @param res Response data
 */
PlaceController.getPlaces = (req, res) => {
    Promise.resolve()
        .then(function() {
            Place.findAll({
                order: [['countryCode', 'ASC']],
            }).then(function(places) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got places!",
                    },
                    data: places,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get places!',
                },
                data: null,
            });
        });
};

/**
 * Get all places by code
 * @param req Request data
 * @param res Response data
 */
PlaceController.getPlacesByCode = (req, res) => {
    Promise.resolve()
        .then(function() {
            Place.findAll({where: {countryCode: req.params.code}}).then(function(places) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got places!",
                    },
                    data: places,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get places!',
                },
                data: null,
            });
        });
};

/**
 * Get place by id
 * @param req Request data
 * @param res Response data
 */
PlaceController.getPlace = (req, res) => {
    Promise.resolve()
        .then(function() {
            Place.findByPk(req.params.id).then(function(place) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got place!",
                    },
                    data: place,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get places!',
                },
                data: null,
            });
        });
};

/**
 * Update place by id (admin only)
 * @param req Request data
 * @param res Response data
 */
PlaceController.updatePlace = (req, res) => {
    Promise.resolve()
        .then(function() {
            Place.update(req.body, {
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Place successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Place update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Place update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete place by id (admin only)
 * @param req Request data
 * @param res Response data
 */
PlaceController.deletePlace = (req, res) => {
    Promise.resolve()
        .then(function() {
            Place.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Place successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Place delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Place delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = PlaceController;
