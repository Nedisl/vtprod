'use strict';

const mail = require('../services/mail');
const Question = require('../models/question');

const QuestionController = {};

/**
 * Add new question
 * @param req Request data
 * @param res Response data
 */
QuestionController.addQuestion = (req, res) => {
    if (!req.body.comment) {
        res.status(200).json({
            meta: {
                success: false,
                message: 'Please provide all needed fields.',
            },
            data: null,
        });
    } else {
        Promise.resolve()
            .then(() => {
                mail.sendNoReplyMessage(
                    'Вопрос',
                    'Телефон: ' +
                        req.body.phone +
                        ', Имя: ' +
                        req.body.phone +
                        ', Комментарий: ' +
                        req.body.comment,
                    '043ru@mail.ru',
                );

                return Question.create(req.body).then(() => {
                    res.status(201).json({
                        meta: {
                            success: true,
                            message: 'Question added!',
                        },
                        data: null,
                    });
                });
            })
            .catch(error => {
                console.log(error);
                res.status(200).json({
                    meta: {
                        success: false,
                        message: 'Error while creating questions!',
                    },
                    data: null,
                });
            });
    }
};

/**
 * Get all questions
 * @param req Request data
 * @param res Response data
 */
QuestionController.getQuestions = (req, res) => {
    Promise.resolve()
        .then(function() {
            Question.findAll({
                order: [['id', 'DESC']],
            }).then(function(questions) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got questions!",
                    },
                    data: {questions},
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get questions!',
                },
                data: null,
            });
        });
};

/**
 * Get question by id
 * @param req Request data
 * @param res Response data
 */
QuestionController.getQuestion = (req, res) => {
    Promise.resolve()
        .then(function() {
            Question.findByPk(req.params.id).then(function(question) {
                res.status(200).json({
                    meta: {
                        success: true,
                        message: "You've got question!",
                    },
                    data: question,
                });
            });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Can not get questions!',
                },
                data: null,
            });
        });
};

/**
 * Update question by id
 * @param req Request data
 * @param res Response data
 */
QuestionController.updateQuestion = (req, res) => {
    Promise.resolve()
        .then(function() {
            Question.update(req.body, {
                where: {
                    id: req.params.id,
                    user: req.user.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Question successfully updated!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Question update failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Question update failed!',
                },
                data: {
                    error,
                },
            });
        });
};

/**
 * Delete question by id (admin only)
 * @param req Request data
 * @param res Response data
 */
QuestionController.deleteQuestion = (req, res) => {
    Promise.resolve()
        .then(function() {
            Question.destroy({
                where: {
                    id: req.params.id,
                },
            })
                .then(function() {
                    res.status(200).json({
                        meta: {
                            success: true,
                            message: 'Question successfully deleted!',
                        },
                        data: null,
                    });
                })
                .catch(function(e) {
                    res.status(200).json({
                        meta: {
                            success: false,
                            message: 'Question delete failed!',
                        },
                        data: {
                            error: e,
                        },
                    });
                });
        })
        .catch(function(error) {
            console.log(error);
            res.status(200).json({
                meta: {
                    success: false,
                    message: 'Question delete failed!',
                },
                data: {
                    error,
                },
            });
        });
};

module.exports = QuestionController;
