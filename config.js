// Application configuration.
'use strict';

const config = module.exports;

// для тестовой разработки
// config.db = {
//     user: 'catkov_tst_vtrs',
//     password: 'TST_vtrs',
//     name: 'catkov_tst_vtrs',
// };

//prod
config.db = {
    user: 'vturs',
    password: 'vturs88',
    name: 'vtprod',
};

// config.db = {
//     user: 'catkov_vturs',
//     password: 'vturs88',
//     name: 'catkov_vturs'
// };

config.db.details = {
    // host: 'catkov.beget.tech', // для тестовой разработки
    host: '45.67.56.70', // prod
    port: 3306,
    dialect: 'mysql',
    charset: 'utf8mb4',
    pool: {max: 5, min: 0, idle: 20000, acquire: 20000},
};

config.keys = {
    secret: '/jVdfUX+u/Kn3qPY4+ahjwQgyV5UhkM5cdh1i2xhozE=',
};

const userRoles = (config.userRoles = {
    user: 1,
    admin: 2,
});

config.accessLevels = {
    user: userRoles.user,
    admin: userRoles.admin,
};
