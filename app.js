const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('passport');
const cors = require('cors');
const apiRouter = require('./routes/api');
const indexRouter = require('./routes/index');
const app = express();
const hookJWTStrategy = require('./services/passportStrategy');
const bodyParser = require('body-parser');

app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '500mb'}));
app.use(bodyParser.urlencoded({limit: '500mb', extended: true, parameterLimit: 50000}));
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

hookJWTStrategy(passport);

app.use('/api', apiRouter(passport));
app.use('/', indexRouter(passport));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send('This method not exists!');
});

(async () => {
    try {
        const db = require('./services/database');

        void require('./models');
        await db.sync();
    } catch (e) {
        console.error(e);
    }
})();

module.exports = app;
