'use strict';

const Office = require('../models/office');

const Offices = {
    getAll: async () => {
        const offices = await Office.findAll();

        return {
            meta: {
                success: true,
                message: "You've got offices!",
            },
            data: offices,
        };
    },

    getById: async id => {
        const office = await Office.findOne({
            where: {
                id,
            },
        });

        return {
            meta: {
                success: true,
                message: "You've got office!",
            },
            data: office,
        };
    },

    getCount: async () => {
        const countOffices = await Office.count();

        return {
            meta: {
                success: true,
                message: "You've got office count!",
            },
            data: countOffices,
        };
    },
};

module.exports = Offices;
