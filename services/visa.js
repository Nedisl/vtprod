'use strict';

const Visa = require('../models/visa');
const Country = require('../models/country');

const Visas = {
    getById: async id => {
        const visa = await Visa.findOne({
            where: {
                id,
            },
            include: [
                {
                    model: Country,
                },
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got visas!",
            },
            data: visa,
        };
    },
    getOneByCountryId: async (countryId, includes) => {
        const visas = await Visa.findOne({
            where: {
                country: countryId,
            },
            attributes: includes && includes.length > 0 ? includes : null,
        });

        return {
            meta: {
                success: true,
                message: "You've got visas!",
            },
            data: visas,
        };
    },
    getAll: async () => {
        const visas = await Visa.findAll({
            include: [
                {
                    model: Country,
                },
            ],
            order: [['title', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got visas!",
            },
            data: visas,
        };
    },

    getPopular: async () => {
        const visas = await Visa.findAll({
            where: {
                isPopular: 1,
            },
            include: [
                {
                    model: Country,
                },
            ],
            order: [['title', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got visas!",
            },
            data: visas,
        };
    },

    getByUrl: async url => {
        const tours = await Visa.findOne({
            where: {
                url,
            },
            include: [
                {
                    model: Country,
                },
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got visas!",
            },
            data: tours,
        };
    },
};

module.exports = Visas;
