'use strict';

const Page = require('../models/page');

const Pages = {
    getAll: async () => {
        const pages = await Page.findAll();

        return {
            meta: {
                success: true,
                message: "You've got pages!",
            },
            data: pages,
        };
    },

    getById: async id => {
        const page = await Page.findOne({
            where: {
                id,
            },
        });

        return {
            meta: {
                success: true,
                message: "You've got page!",
            },
            data: page,
        };
    },
};

module.exports = Pages;
