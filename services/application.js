'use strict';

const ApplicationType = require('../models/applicationType');

const Applications = {
    getApplicationTypeById: async id => {
        const types = await ApplicationType.findOne({
            where: {
                id,
            },
        });

        return {
            meta: {
                success: true,
                message: "You've got reviews!",
            },
            data: types,
        };
    },
};

module.exports = Applications;
