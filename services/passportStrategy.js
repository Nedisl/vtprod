'use strict';

const JWTStrategy = require('passport-jwt').Strategy;

const {ExtractJwt} = require('passport-jwt');

const User = require('./../models/user');

const config = require('./../config');

const hookJWTStrategy = passport => {
    const options = {
        secretOrKey: config.keys.secret,
        jwtFromRequest: ExtractJwt.fromAuthHeader(),
        ignoreExpiration: false,
    };

    passport.use(
        new JWTStrategy(options, function(JWTPayload, callback) {
            User.findOne({where: {email: JWTPayload.email}}).then(function(user) {
                if (!user) {
                    callback(null, false);

                    return;
                }

                callback(null, user);
            });
        }),
    );
};

module.exports = hookJWTStrategy;
