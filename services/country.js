'use strict';

const Country = require('../models/country');
const Quote = require('../models/quote');
const Team = require('../models/team');

const Countries = {
    getAl: async () => {
        const countries = await Country.findAll({
            where: {
                available: 1,
            },
            order: [['name', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got countries!",
            },
            data: countries,
        };
    },

    getList: async includes => {
        const countries = await Country.findAll({
            where: {
                available: 1,
            },
            attributes:
                includes && includes.length > 0
                    ? includes
                    : {
                          exclude: [
                              'lat',
                              'lng',
                              'guide',
                              'blockVisa',
                              'hotels',
                              'weather',
                              'background',
                          ],
                      },
            order: [['name', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got countries!",
            },
            data: countries,
        };
    },

    getById: async id => {
        const country = await Country.findOne({
            where: {
                id,
            },
            include: [
                {
                    model: Quote,
                    include: [Team],
                },
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got country!",
            },
            data: country,
        };
    },

    getByUrl: async (url, includes) => {
        const country = await Country.findOne({
            where: {
                url,
            },
            attributes: includes && includes.length > 0 ? includes : null,
            include: [
                {
                    model: Quote,
                    include: [Team],
                },
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got country!",
            },
            data: country,
        };
    },

    getByCode: async code => {
        const country = await Country.findOne({
            where: {
                code,
            },
            include: [
                {
                    model: Quote,
                    include: [Team],
                },
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got country!",
            },
            data: country,
        };
    },

    getPopular: async () => {
        const countries = await Country.findAll({
            where: {
                isPopular: 1,
                available: 1,
            },
            order: [['name', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got countries!",
            },
            data: countries,
        };
    },

    getPopularList: async () => {
        const countries = await Country.findAll({
            where: {
                isPopular: 1,
                available: 1,
            },
            attributes: {
                exclude: ['lat', 'lng', 'guide', 'blockVisa', 'hotels', 'weather'],
            },
            order: [['name', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got countries!",
            },
            data: countries,
        };
    },

    getSelected: async () => {
        const countries = await Country.findAll({
            where: {
                selected: 1,
                available: 1,
            },
            attributes: {
                exclude: ['lat', 'lng', 'guide', 'blockVisa', 'hotels', 'weather'],
            },
            order: [['name', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got countries!",
            },
            data: countries,
        };
    },
};

module.exports = Countries;
