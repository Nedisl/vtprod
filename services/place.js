'use strict';

const Place = require('../models/place');

const Places = {
    getByCode: async (code, includes) => {
        const places = await Place.findAll({
            where: {
                countryCode: code,
                isOutput: 1,
            },
            attributes: includes && includes.length > 0 ? includes : null,
        });

        return {
            meta: {
                success: true,
                message: "You've got places!",
            },
            data: places,
        };
    },

    getById: async id => {
        const places = await Place.findByPk(id);

        return {
            meta: {
                success: true,
                message: "You've got place!",
            },
            data: places,
        };
    },

    getByUrl: async url => {
        const places = await Place.findOne({
            where: {
                url,
            },
        });

        return {
            meta: {
                success: true,
                message: "You've got place!",
            },
            data: places,
        };
    },
};

module.exports = Places;
