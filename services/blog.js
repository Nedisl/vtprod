'use strict';

const Blog = require('../models/blog');
const Sequelize = require('sequelize');

const Blogs = {
    getAll: async () => {
        const blogs = await Blog.findAll({
            where: {
                fixed: 0,
            },
            order: [['id', 'DESC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got blogs!",
            },
            data: blogs,
        };
    },

    getById: async id => {
        const blog = await Blog.findOne({
            where: {
                id,
            },
        });

        return {
            meta: {
                success: true,
                message: "You've got blogs!",
            },
            data: blog,
        };
    },

    getRandomBlogs: async id => {
        const blog = await Blog.findAll({
            where: {
                id: {
                    ne: id,
                },
            },
            order: Sequelize.literal('rand()'),
            limit: 4,
        });

        return {
            meta: {
                success: true,
                message: "You've got blog!",
            },
            data: blog,
        };
    },

    getByUrl: async url => {
        const blogs = await Blog.findOne({
            where: {
                url,
            },
        });

        return {
            meta: {
                success: true,
                message: "You've got blog!",
            },
            data: blogs,
        };
    },
};

module.exports = Blogs;
