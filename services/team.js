'use strict';

const Team = require('../models/team');
const Office = require('../models/office');

const Teams = {
    getAll: async () => {
        const team = await Team.findAll({
            include: [
                {
                    model: Office,
                },
            ],
            order: [['position', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got reviews!",
            },
            data: team,
        };
    },
};

module.exports = Teams;
