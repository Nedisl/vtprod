'use strict';

const Month = require('../models/month');

const Months = {
    getByCountryAndMonthIndex: async (countryId, monthIndex) => {
        const months = await Month.findOne({
            where: {
                country: countryId,
                month: monthIndex,
            },
        });

        return {
            meta: {
                success: true,
                message: "You've got months!",
            },
            data: months,
        };
    },

    getByCountry: async (id, includes) => {
        const months = await Month.findAll({
            where: {
                country: id,
                isOutput: 1,
            },
            attributes: includes && includes.length > 0 ? includes : null,
            order: [['month', 'ASC']],
        });

        return {
            meta: {
                success: true,
                message: "You've got months!",
            },
            data: months,
        };
    },

    getById: async id => {
        const months = await Month.findByPk(id);

        return {
            meta: {
                success: true,
                message: "You've got months!",
            },
            data: months,
        };
    },
    getByUrl: async url => {
        const months = await Month.findOne({
            where: {
                url,
            },
        });

        return {
            meta: {
                success: true,
                message: "You've got month!",
            },
            data: months,
        };
    },
};

module.exports = Months;
