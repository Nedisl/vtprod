const nodemailer = require('nodemailer');

module.exports = {
    sendNoReplyMessage: (messageTopic, messageText, userEmail) => {
        const transporter = nodemailer.createTransport({
            host: 'smtp.yandex.ru',
            port: 465,
            secure: true,
            auth: {
                user: 'vyatkatour.notifier@yandex.ru',
                pass: 'vturs88',
            },
        });

        const mailOptions = {
            from: '"ВяткаТур" <vyatkatour.notifier@yandex.ru>',
            to: userEmail,
            subject: messageTopic,
            text: messageText,
            html: messageText,
        };

        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
            }
            console.log('Message sent');
        });
    },
};
