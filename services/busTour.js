'use strict';

const BusTour = require('../models/busTour');

const BusTours = {
    getTour: async () => {
        const tours = await BusTour.findByPk(1);

        return {
            meta: {
                success: true,
                message: "You've got tour!",
            },
            data: tours,
        };
    },
};

module.exports = BusTours;
