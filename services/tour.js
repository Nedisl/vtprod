'use strict';

const Tour = require('../models/tour');
const TourType = require('../models/tourType');
const Place = require('../models/place');
const Hotel = require('../models/hotel');
const sequelize = require('sequelize');

const {Op} = sequelize;

const include = [
    {
        model: TourType,
    },
    {
        model: Place,
    },
    {
        model: Hotel,
    },
];

const Tours = {
    getSelected: async () => {
        const tours = await Tour.findAll({
            where: {
                selected: 1,
            },
            include,
            order: [
                ['priority', 'DESC'],
                ['name', 'ASC'],
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got tours!",
            },
            data: tours,
        };
    },

    getAll: async () => {
        const tours = await Tour.findAll({
            include,
            order: [
                ['priority', 'DESC'],
                ['name', 'ASC'],
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got tours!",
            },
            data: tours,
        };
    },

    getBusSelected: async () => {
        const tours = await Tour.findAll({
            where: {
                busSelected: 1,
            },
            include,
            order: [
                ['priority', 'DESC'],
                ['name', 'ASC'],
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got tours!",
            },
            data: tours,
        };
    },

    getBanner: async () => {
        const tours = await Tour.findOne({
            where: {
                banner: {
                    [Op.ne]: null,
                },
            },
            include,
        });

        return {
            meta: {
                success: true,
                message: "You've got tours!",
            },
            data: tours,
        };
    },

    getById: async id => {
        const tours = await Tour.findOne({
            where: {
                id,
            },
            include,
        });

        return {
            meta: {
                success: true,
                message: "You've got tours!",
            },
            data: tours,
        };
    },

    getByUrl: async url => {
        const tours = await Tour.findOne({
            where: {
                url,
            },
            include,
        });

        return {
            meta: {
                success: true,
                message: "You've got tours!",
            },
            data: tours,
        };
    },
};

module.exports = Tours;
