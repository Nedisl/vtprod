'use strict';

const Review = require('../models/review');
const Country = require('../models/country');
const moment = require('moment');
const Sequelize = require('sequelize');
const {Op} = Sequelize;

const Reviews = {
    getAll: async req => {
        const offset = +req.query.offset;
        const limit = +req.query.limit;

        if (req.query.date) {
            const date = moment(req.query.date);
            const nextDate = moment(req.query.date);

            nextDate.add(1, 'months');
            delete req.query.date;
            req.query.date = {
                [Op.and]: [
                    {
                        [Op.gte]: date.format(),
                    },
                    {
                        [Op.lt]: nextDate.format(),
                    },
                ],
            };
        }

        delete req.query.limit;
        delete req.query.offset;

        const reviews = await Review.findAll({
            where: req.body,
            include: [
                {
                    model: Country,
                },
            ],
            order: [['date', 'DESC']],
            limit: limit + 1,
            offset,
        });

        let isLastPage = false;

        if (reviews.length < limit + 1) {
            isLastPage = true;
        } else {
            reviews.pop();
        }

        return {
            meta: {
                success: true,
                message: "You've got reviews!",
                isLastPage,
            },
            data: reviews,
        };
    },

    getAllPure: async () => {
        const reviews = await Review.findAll({
            where: {
                isPublished: 1,
            },
            order: [['date', 'DESC']],
            include: [
                {
                    model: Country,
                },
            ],
            limit: 10,
        });

        return {
            meta: {
                success: true,
                message: "You've got reviews!",
            },
            data: reviews,
        };
    },

    getAllByCountryId: async (id, isNeedCountry = true, includes) => {
        const reviews = await Review.findAll({
            where: {
                country: id,
            },
            attributes: includes && includes.length > 0 ? includes : null,
            include: isNeedCountry
                ? [
                      {
                          model: Country,
                      },
                  ]
                : null,
            order: [['date', 'DESC']],
            limit: 10,
        });

        return {
            meta: {
                success: true,
                message: "You've got reviews!",
            },
            data: reviews,
        };
    },
};

module.exports = Reviews;
