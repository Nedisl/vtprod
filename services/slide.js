'use strict';

const Slide = require('../models/sliderMain');
const Country = require('../models/country');

const Slides = {
    getAll: async () => {
        const slides = await Slide.findAll({
            include: [
                {
                    model: Country,
                },
            ],
        });

        return {
            meta: {
                success: true,
                message: "You've got slides!",
            },
            data: slides,
        };
    },
};

module.exports = Slides;
