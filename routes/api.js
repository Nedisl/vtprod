'use strict';

const router = require('express').Router();

const config = require('../config');

const {allowOnly} = require('../services/routesHelper');
const {HotelsController} = require('../controllers/hotelsController');
const AuthController = require('../controllers/authController');
const TourController = require('../controllers/tourController');
const BusTourController = require('../controllers/busTourController');
const CountryController = require('../controllers/countryController');
const ReviewController = require('../controllers/reviewController');
const PlaceController = require('../controllers/placeController');
const QuestionController = require('../controllers/questionController');
const ApplicationController = require('../controllers/applicationController');
const BlogController = require('../controllers/blogController');
const BlockController = require('../controllers/blockController');
const VisaController = require('../controllers/visaController');
const UserController = require('../controllers/userController');
const OfficeController = require('../controllers/officeController');
const SliderMainController = require('../controllers/sliderMainController');
const SubscriberController = require('../controllers/subscriberController');
const IconController = require('../controllers/iconController');
const FeedbackController = require('../controllers/feedbackController');
const CalculationController = require('../controllers/calculationController');
const RateController = require('../controllers/rateController');
const TeamController = require('../controllers/teamController');
const CityController = require('../controllers/cityController');
const QuoteController = require('../controllers/quoteController');
const PageController = require('../controllers/pageController');
const MonthController = require('../controllers/monthController');

const APIRoutes = passport => {
    //russian hotels
    router.get('/hotel/:id', HotelsController.get);
    router.get('/hotel', HotelsController.getAll);
    router.delete(
        '/hotel/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, HotelsController.delete),
    );
    router.post(
        '/hotel',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, HotelsController.add),
    );
    router.put(
        '/hotel/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, HotelsController.update),
    );

    // Common
    router.get('/tours', TourController.getTours);
    router.get('/tours/selected', TourController.getSelectedTours);
    router.get('/tour/:id', TourController.getTour);
    router.get('/bus/tour/:id', BusTourController.getTour);
    router.get('/countries/all', CountryController.getCountries);
    router.get('/country/:id', CountryController.getCountry);
    router.get('/country/admin/:id', CountryController.getAdminCountry);
    router.get('/country/photos/:id', CountryController.getCountryPhotos);
    router.get('/country/photo/:id', CountryController.getCountryPhoto);
    router.get('/countries/popular', CountryController.getPopularCountries);
    router.get('/countries/admin', CountryController.getAdminCountries);
    router.get('/countries/selected', CountryController.getSelectedCountries);
    router.get('/places', PlaceController.getPlaces);
    router.get('/countries/places/:code', PlaceController.getPlacesByCode);
    router.get('/place/:id', PlaceController.getPlace);
    router.get('/country/published/reviews/:id', ReviewController.getCountryReviews);
    router.get('/reviews', ReviewController.getAllReviews);
    router.get('/country/reviews', ReviewController.getAllReviews);
    router.get('/country/reviews', ReviewController.getAllReviews);
    router.get('/tour/photos/:id', TourController.getTourPhotos);
    router.get('/tour/photo/:id', TourController.getTourPhoto);
    router.get('/tour/list/type', TourController.getTourTypes);
    router.get('/review/:id', ReviewController.getReview);
    router.get('/question', QuestionController.getQuestions);
    router.get('/feedback', FeedbackController.getFeedbacks);
    router.get('/question/:id', QuestionController.getQuestion);
    router.get('/application', ApplicationController.getApplications);
    router.get('/application/:id', ApplicationController.getApplication);
    router.get('/user/application/:id', ApplicationController.getUserApplications);
    router.get('/tour/application/:id', ApplicationController.getTourApplications);
    router.get('/blog', BlogController.getBlogs);
    router.get('/city', CityController.getCities);
    router.get('/city/:id', CityController.getCity);
    router.get('/random/blog/:id', BlogController.getRandomBlogs);
    router.get('/blog/:id', BlogController.getBlog);
    router.get('/blog/fixed/:fixed', BlogController.getFixedBlogs);
    router.get('/blog/photos/:id', BlogController.getBlogPhotos);
    router.get('/block', BlockController.getBlocks);
    router.get('/block/:id', BlockController.getBlock);
    router.get('/quote', QuoteController.getQuotes);
    router.get('/quote/:id', QuoteController.getQuote);
    router.get('/visa', VisaController.getVisas);
    router.get('/visas/popular', VisaController.getPopularVisas);
    router.get('/visas/country/:id', VisaController.getVisaByCountry);
    router.get('/visa/:id', VisaController.getVisa);
    router.get('/office', OfficeController.getOffices);
    router.get('/office/:id', OfficeController.getOffice);
    router.post('/question', QuestionController.addQuestion);
    router.post('/feedback', FeedbackController.addFeedback);
    router.post('/calculation', CalculationController.addCalculation);
    router.get('/calculation', CalculationController.getCalculations);
    router.get('/calculation/:id', CalculationController.getCalculation);
    router.post('/rate', RateController.addRate);
    router.get('/rate', RateController.getRates);
    router.get('/rate/:id', RateController.getRate);
    router.get('/slider/main', SliderMainController.getSlides);
    router.get('/slider/main/:id', SliderMainController.getSlide);
    router.post('/subscribers', SubscriberController.addSubscriber);
    router.get('/subscribers', SubscriberController.getSubscribers);
    router.get('/icon', IconController.getIcons);
    router.get('/team', TeamController.getTeam);
    router.get('/team/:id', TeamController.getTeamMember);
    router.get('/page', PageController.getPages);
    router.get('/page/:id', PageController.getPage);
    router.get('/month/:month', MonthController.getMonthsByNumberAndCountry);
    router.get('/month', MonthController.getMonths);
    router.get('/country/month/:country', MonthController.getMonthsByCountry);

    // User
    router.post('/auth/signup', AuthController.signUp);
    router.post('/auth/login', AuthController.authenticateUser);
    router.post('/auth/check/password', AuthController.checkPassword);
    router.get(
        '/profile',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.user, UserController.getProfile),
    );
    router.post('/review', ReviewController.addReview);
    router.delete(
        '/question/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, QuestionController.deleteQuestion),
    );
    router.post('/application', ApplicationController.addApplication);
    router.put(
        '/application/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, ApplicationController.updateApplication),
    );
    router.delete(
        '/application/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, ApplicationController.deleteApplication),
    );

    // Admin
    router.put(
        '/review/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, ReviewController.updateReview),
    );
    router.delete(
        '/review/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, ReviewController.deleteReview),
    );
    router.post(
        '/bus/tour',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BusTourController.addTour),
    );
    router.put(
        '/bus/tour/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BusTourController.updateTour),
    );
    router.delete(
        '/bus/tour/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BusTourController.deleteTour),
    );
    router.post(
        '/tour',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TourController.addTour),
    );
    router.put(
        '/tour/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TourController.updateTour),
    );
    router.delete(
        '/tour/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TourController.deleteTour),
    );
    router.post(
        '/tour/photo',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TourController.addTourPhoto),
    );
    router.delete(
        '/tour/photo/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TourController.deleteTourPhoto),
    );
    router.post(
        '/tour/type',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TourController.addTourType),
    );
    router.delete(
        '/tour/type/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TourController.deleteTourType),
    );
    router.post(
        '/country/photo',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CountryController.addCountryPhoto),
    );
    router.delete(
        '/country/photo/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CountryController.deleteCountryPhoto),
    );
    router.post(
        '/country',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CountryController.addCountry),
    );
    router.put(
        '/country/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CountryController.updateCountry),
    );
    router.delete(
        '/country/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CountryController.deleteCountry),
    );
    router.post(
        '/place',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, PlaceController.addPlace),
    );
    router.put(
        '/place/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, PlaceController.updatePlace),
    );
    router.delete(
        '/place/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, PlaceController.deletePlace),
    );
    router.post(
        '/blog',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BlogController.addBlog),
    );
    router.put(
        '/blog/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BlogController.updateBlog),
    );
    router.delete(
        '/blog/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BlogController.deleteBlog),
    );
    router.delete(
        '/blog/photo/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BlogController.deleteBlogPhoto),
    );
    router.post(
        '/blog/photo',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BlogController.addBlogPhoto),
    );
    router.post(
        '/block',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BlockController.addBlock),
    );
    router.put(
        '/block/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BlockController.updateBlock),
    );
    router.delete(
        '/block/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, BlockController.deleteBlock),
    );
    router.post(
        '/quote',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, QuoteController.addQuote),
    );
    router.put(
        '/quote/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, QuoteController.updateQuote),
    );
    router.delete(
        '/quote/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, QuoteController.deleteQuote),
    );
    router.post(
        '/visa',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, VisaController.addVisa),
    );
    router.put(
        '/visa/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, VisaController.updateVisa),
    );
    router.delete(
        '/visa/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, VisaController.deleteVisa),
    );
    router.post(
        '/visa/photo',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, VisaController.addVisaPhoto),
    );
    router.delete(
        '/visa/photo/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, VisaController.deleteVisaPhoto),
    );
    router.post(
        '/office',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, OfficeController.addOffice),
    );
    router.put(
        '/office/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, OfficeController.updateOffice),
    );
    router.delete(
        '/office/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, OfficeController.deleteOffice),
    );
    router.post(
        '/slider/main',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, SliderMainController.addSlide),
    );
    router.delete(
        '/slider/main/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, SliderMainController.deleteSlide),
    );
    router.put(
        '/slider/main/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, SliderMainController.updateSlider),
    );
    router.post(
        '/icon',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, IconController.addIcon),
    );
    router.delete(
        '/icon/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, SliderMainController.deleteSlide),
    );
    router.put(
        '/team/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TeamController.updateTeamMember),
    );
    router.post(
        '/team',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TeamController.addTeam),
    );
    router.delete(
        '/team/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, TeamController.deleteTeamMember),
    );
    router.post(
        '/city',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CityController.addCity),
    );
    router.put(
        '/city/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CityController.updateCity),
    );
    router.delete(
        '/city/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CityController.deleteCity),
    );
    router.delete(
        '/calculation/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, CalculationController.deleteCalculation),
    );
    router.put(
        '/page/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, PageController.updatePage),
    );
    router.post(
        '/page',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, PageController.addPage),
    );
    router.delete(
        '/page/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, PageController.deletePage),
    );
    router.post(
        '/month',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, MonthController.addMonth),
    );
    router.put(
        '/month/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, MonthController.updateMonth),
    );
    router.delete(
        '/month/:id',
        passport.authenticate('jwt', {session: false}),
        allowOnly(config.accessLevels.admin, MonthController.deleteMonth),
    );

    return router;
};

module.exports = APIRoutes;
