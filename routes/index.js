'use strict';

const router = require('express').Router();

const ReviewsController = require('../controllers/pages/reviewsController');
const IndexController = require('../controllers/pages/indexController');
const AboutController = require('../controllers/pages/aboutController');
const BlogController = require('../controllers/pages/blogController');
const BonusesController = require('../controllers/pages/bonusesController');
const BusController = require('../controllers/pages/busController');
const BusTourController = require('../controllers/pages/busTourController');
const ContactsController = require('../controllers/pages/contactsController');
const CountryController = require('../controllers/pages/countryController');
const InstalmentsController = require('../controllers/pages/instalmentsController');
const PostController = require('../controllers/pages/postController');
const TourController = require('../controllers/pages/tourController');
const VisaController = require('../controllers/pages/visaController');
const VisasController = require('../controllers/pages/visasController');
const InitController = require('../controllers/pages/initController');
const PlaceController = require('../controllers/pages/placeController');
const MonthController = require('../controllers/pages/monthController');

const indexRoutes = passport => {
    router.get('/reviews', ReviewsController.getReviewsPage);
    router.get('/', IndexController.getIndexPage);
    router.get('/blog', BlogController.getBlogPage);
    router.get('/about', AboutController.getAboutPage);
    router.get('/bonuses', BonusesController.getBonusesPage);
    router.get('/bus', BusController.getBusPage);
    router.get('/contacts', ContactsController.getContactsPages);
    router.get('/bus/tours', BusTourController.getBusPage);
    router.get('/country/:id', CountryController.getCountryPage);
    router.get('/strany/:url', CountryController.getCountryPageByUrl);
    router.get('/instalments', InstalmentsController.getInstalmentsPage);
    router.get('/site-map', IndexController.getSiteMap);
    router.get('/post/:id', PostController.getPostPage);
    router.get('/tour/:id', TourController.getTourPage);
    router.get('/tur/:url', TourController.getTourPageUrl);
    router.get('/visa/:id', VisaController.getVisaPage);
    router.get('/visy/:url', VisaController.getVisaPageByUrl);
    router.get('/visas', VisasController.getVisasPage);
    router.get('/place/:id', PlaceController.getPlace);
    router.get('/month/:id', MonthController.getMonth);
    router.get('/controller/place/:url', PlaceController.getPlace);
    router.get(
        '/controller/country/:countryUrl/month/:monthIndex',
        MonthController.getMonth,
    );
    router.get('/controller/index', IndexController.getIndexPage);
    router.get('/controller/reviews', ReviewsController.getReviewsPage);
    router.get('/controller/blog', BlogController.getBlogPage);
    router.get('/controller/site-map', IndexController.getSiteMap);
    router.get('/controller/about', AboutController.getAboutPage);
    router.get('/controller/bonuses', BonusesController.getBonusesPage);
    router.get('/controller/bus', BusController.getBusPage);
    router.get('/controller/contacts', ContactsController.getContactsPages);
    router.get('/controller/country/:id', CountryController.getCountryPage);
    router.get('/controller/instalments', InstalmentsController.getInstalmentsPage);
    router.get('/controller/post/:id', PostController.getPostPage);
    router.get('/controller/tour/:id', TourController.getTourPage);
    router.get('/controller/visa/:id', VisaController.getVisaPage);
    router.get('/controller/visas', VisasController.getVisasPage);
    router.get('/controller/init', InitController.getInitPage);
    router.get('/controller/bus/tours', BusTourController.getBusPage);

    return router;
};

module.exports = indexRoutes;
